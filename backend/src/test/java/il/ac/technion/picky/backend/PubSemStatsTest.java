package il.ac.technion.picky.backend;

import org.junit.Test;

import il.ac.technion.picky.backend.entity.PubSemStats;
import il.ac.technion.picky.backend.type.PubReport;

import static il.ac.technion.picky.backend.Enums.Moed;
import static il.ac.technion.picky.backend.Enums.Source;
import static org.junit.Assert.assertEquals;

public class PubSemStatsTest {

    @Test
    public void testApprove() throws Exception {
        PubReport pubReport1 = new PubReport(Source.CSExcel, Moed.A, 90);
        PubReport pubReport2 = new PubReport(Source.CSExcel, Moed.B, 60);
        PubSemStats pubSemStats = new PubSemStats();
        pubSemStats.addApproved(pubReport1);
        pubSemStats.addApproved(pubReport2);
        assertEquals(80, pubSemStats.getAverage(), 0);
    }

    @Test
    public void testApproveAllMoeds() throws Exception {
        PubSemStats pubSemStats = new PubSemStats();
        pubSemStats.addApproved(new PubReport(Source.CSExcel, Moed.A, 90));
        pubSemStats.addApproved(new PubReport(Source.CSExcel, Moed.B, 60));
        pubSemStats.addApproved(new PubReport(Source.CSExcel, Moed.C, 70));
        pubSemStats.addApproved(new PubReport(Source.CSExcel, Moed.GENERAL, 80));
        assertEquals(79.23, pubSemStats.getAverage(), 0.001);
    }

    @Test
    public void testDeny() throws Exception {
        PubSemStats pubSemStats = new PubSemStats();
        pubSemStats.addDenied(new PubReport(Source.CSExcel, Moed.A, 90));
        pubSemStats.addApproved(new PubReport(Source.CSExcel, Moed.B, 60));
        assertEquals(60, pubSemStats.getAverage(), 0);
    }

}