package il.ac.technion.picky.backend;

import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.Closeable;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import il.ac.technion.picky.backend.entity.PubReportEntity;
import il.ac.technion.picky.backend.entity.PubStats;
import il.ac.technion.picky.backend.entity.Student;
import il.ac.technion.picky.backend.entity.UgStats;
import il.ac.technion.picky.backend.type.AppStats;
import il.ac.technion.picky.backend.type.CourseStats;
import il.ac.technion.picky.backend.type.PubReport;
import il.ac.technion.picky.backend.type.SemesterPubReport;

import static il.ac.technion.picky.backend.Enums.Moed;
import static il.ac.technion.picky.backend.Enums.Source;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class PickyAPITest {

    private static final PickyAPI API = new PickyAPI();
    private static final String STUDENT_ID = "1";
    private static final Long COURSE_ID = 234114L;
    private static final User USER = new User("", "");
    // for pub grade assertEquals
    private static final double DELTA = 0.01;


    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig().setApplyAllHighRepJobPolicy());
    private Closeable session;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        helper.setUp();
        session = ObjectifyService.begin();
    }

    @After
    public void tearDown() throws Exception {
        helper.tearDown();
        session.close();
    }

    @Test
    public void testGetStudent() throws Exception {
        assertNull(API.getStudent(STUDENT_ID, USER));
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[234114,65],[234114,75]]]]", USER);
        Student student = API.getStudent(STUDENT_ID, USER);
        assertNotNull(student);
        assertEquals(2, student.getGradesNumber());
        assertEquals(20121L, (long) student.getLastUpdate());
    }

    @Test
    public void testDeleteStudent() throws Exception {
        assertNull(API.getStudent(STUDENT_ID, USER));
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[234114,65],[234114,75]]]]", USER);
        assertNotNull(API.getStudent(STUDENT_ID, USER));
        API.deleteStudent(STUDENT_ID, USER);
        assertNull(API.getStudent(STUDENT_ID, USER));
    }

    @Test
    public void testDeleteStudentWithNoUser() throws Exception {
        thrown.expect(UnauthorizedException.class);
        API.addUgGrades(STUDENT_ID, "", null);
    }

    @Test
    public void testAddUgGrade() throws Exception {
        API.addUgGrade(COURSE_ID, 95, 2015, 1, USER);
        assertEquals(95.0, API.getUgStats(COURSE_ID, USER).getAverage(),DELTA);
    }

    @Test
    public void testAddUgGrades() throws Exception {
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[234114,65],[234114,75]]]]", USER);
        assertEquals(70.0, API.getUgStats(234114L, USER).getAverage(),DELTA);
    }

    @Test
    public void testAddUgGradesWithUpdate() throws Exception {
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[104012,65],[44145,70],[104167,60],[104167,80],[234114,95]]]]", USER);
        Student student = API.getStudent(STUDENT_ID, USER);
        assertEquals(5, student.getGradesNumber());
        assertEquals(65.0, API.getUgStats(104012L, USER).getAverage(),DELTA);
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[104012,65],[44145,70],[104167,60],[104167,80],[234114,95]]],[2012,2,[[104012,85],[114071,55],[114071,65],[234118,75],[234122,90],[234141,90]]]]", USER);
        student = API.getStudent(STUDENT_ID, USER);
        assertEquals(11, student.getGradesNumber());
        assertEquals(75.0, API.getUgStats(104012L, USER).getAverage(),DELTA);
    }

    @Test
    public void testUpdateLimit() throws Exception {
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[234114,65],[234114,75]]],[2050,1,[[234122,80]]]]", USER);
        assertEquals(2, API.getStudent(STUDENT_ID, USER).getGradesNumber());
    }

    @Test
    public void testGetUgStats() throws Exception {
        API.addUgGrade(234114L, 95, 2015, 1, USER);
        assertEquals(95.0, API.getUgStats(234114L, USER).getAverage(),DELTA);
    }

    @Test
    public void testGetUgGrades() throws Exception {
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[234114,65],[234114,75]]]]", USER);
        assertEquals(Arrays.asList(65.0, 75.0), API.getUgGrades(234114L, USER));
    }

    @Test
    public void testAddWaitingPubReport() throws Exception {
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 60, 2012, 1, Moed.A, "", "", USER);
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 60, 2012, 2, Moed.A, "", "", USER);
        // same info but different auto-generated id
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 60, 2012, 2, Moed.A, "", "", USER);
        assertEquals(3, API.getWaitingPubReports(USER, null).size());
    }

    @Test
    public void testGetWaitingPubReportWithLimit() throws Exception {
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 60, 2012, 1, Moed.A, "", "", USER);
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 60, 2012, 2, Moed.A, "", "", USER);
        // same info but different auto-generated id
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 60, 2012, 2, Moed.A, "", "", USER);
        assertEquals(2, API.getWaitingPubReports(USER, 2).size());
    }

    @Test
    public void testApprovePubReport() throws Exception {
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 62.5, 2012, 1, Moed.A, "", "", USER);
        API.approvePubReport(API.getWaitingPubReports(USER, null).get(0).getId(), USER);
        assertEquals(62.5, API.getPubStats(COURSE_ID, USER).getAverage(),DELTA);
        assertEquals(0, API.getWaitingPubReports(USER, null).size());
    }

    @Test
    public void testDenyPubReport() throws Exception {
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 62.5, 2012, 1, Moed.A, "", "", USER);
        API.denyPubReport(API.getWaitingPubReports(USER, null).get(0).getId(), USER);
        assertNull(API.getPubStats(COURSE_ID, USER));
        assertEquals(0, API.getWaitingPubReports(USER, null).size());
    }


    @Test
     public void testGetStats() throws Exception {
        // ug
        API.addUgGrade(COURSE_ID, 95, 2015, 1, USER);
        // pub
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 62.5, 2012, 1, Moed.A, "", "", USER);
        API.approvePubReport(API.getWaitingPubReports(USER, null).get(0).getId(), USER);
        CourseStats stats = API.getStats(COURSE_ID, USER);
        assertEquals(95.0, stats.getUgStats().getAverage(),DELTA);
        assertEquals(62.5, stats.getPubStats().getAverage(),DELTA);
    }

    @Test
    public void testGetStatsOnlyUgExists() throws Exception {
        API.addUgGrade(COURSE_ID, 95, 2015, 1, USER);
        CourseStats stats = API.getStats(COURSE_ID, USER);
        assertEquals(95.0, stats.getUgStats().getAverage(),DELTA);
        assertNull(stats.getPubStats());
    }

    @Test
    public void testGetAppStats() throws Exception {
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[104012,65],[44145,70],[104167,60],[104167,80],[234114,95]]]]", USER);
        API.addWaitingPubReport(Source.CSExcel, 234122L, 70, 2012, 1, Moed.A, "", "", USER);
        API.approvePubReport(API.getWaitingPubReports(USER, null).get(0).getId(), USER);
        AppStats stats  = API.getAppStats(USER);
        assertEquals(1, stats.getStudentsNum());
        assertEquals(5, stats.getUgGradesNum());
        assertEquals(4, stats.getCoursesWithUgInfoNum());
        assertEquals(1, stats.getCoursesWithPubInfoNum());
        assertEquals(5, stats.getCoursesWithInfoNum());
        assertEquals(74.0, stats.getUgGradesAvg(),DELTA);
        assertEquals(70.0, stats.getPubGradesAvg(),DELTA);
    }

    @Test
    public void testApprovePubReportMultiMoeds() throws Exception {
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 60, 2012, 1, Moed.A, "", "", USER);
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 70, 2012, 1, Moed.B, "", "", USER);
        for (PubReportEntity r : API.getWaitingPubReports(USER, null))
            API.approvePubReport(r.getId(), USER);
        PubStats stats = API.getStats(COURSE_ID, USER).getPubStats();
        // count semesters
        assertEquals(1, stats.getCount());
        assertEquals((60*Moed.A.weight()+70*Moed.B.weight())/(double)(Moed.A.weight() + Moed.B.weight()), stats.getAverage(),DELTA);
    }

    @Test
    public void testUgStatsSameAndDifferentSemesters() throws Exception {
        API.addUgGrade(COURSE_ID, 95, 2015, 1, USER);
        API.addUgGrade(COURSE_ID, 85, 2015, 2, USER);
        API.addUgGrade(COURSE_ID, 75, 2015, 2, USER);
        UgStats stats = API.getStats(COURSE_ID, USER).getUgStats();
        assertEquals(3, stats.getCount());
        assertEquals(85.0, stats.getAverage(),DELTA);
        assertEquals(new HashSet<>(Arrays.asList(23411420152L, 23411420151L)), stats.inGetSemesters());
        assertEquals(Arrays.asList(85.0, 75.0, 95.0), stats.inGetGrades());
    }

    @Test
    public void testGetApprovedPubReports() throws Exception {
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 60, 2012, 1, Moed.A, "", "", USER);
        API.addWaitingPubReport(Source.CSExcel, COURSE_ID, 70, 2012, 1, Moed.B, "", "", USER);
        for (PubReportEntity r : API.getWaitingPubReports(USER, null))
            API.approvePubReport(r.getId(), USER);
        List<SemesterPubReport> res = API.getApprovedPubReports(COURSE_ID, USER);
        assertEquals(new Long(20121), res.get(0).getSemester());
        List<PubReport> reports = res.get(0).getPubReport();
        assertEquals(2, reports.size());
        assertEquals(130.0, reports.get(0).getFinalAvg()+reports.get(1).getFinalAvg(), DELTA);
    }

    @Test
    public void testUpdateLimitWithGetStudent() throws Exception {
        assertNull(API.getStudent(STUDENT_ID, USER));
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[234114,65],[234114,75]]],[2050,1,[[234122,80]]]]", USER);
        assertEquals(2, API.getStudent(STUDENT_ID, USER).getGradesNumber());
        Student student = API.getStudent(STUDENT_ID, USER);
        assertNotNull(student);
        assertEquals(2, student.getGradesNumber());
        assertNotEquals(20501L, (long) student.getLastUpdate());
    }

    @Test
    public void testNewStudentUpdateLimitNotCreatedThenCreated() throws Exception {
        assertNull(API.getStudent(STUDENT_ID, USER));
        API.addUgGrades(STUDENT_ID, "[[2050,1,[[234122,80]]]]", USER);
        assertNull( API.getStudent(STUDENT_ID, USER));
        API.addUgGrades(STUDENT_ID, "[[2012,1,[[234114,65],[234114,75]]]]", USER);
        assertEquals(2, API.getStudent(STUDENT_ID, USER).getGradesNumber());
        Student student = API.getStudent(STUDENT_ID, USER);
        assertNotNull(student);
        assertEquals(2, student.getGradesNumber());
        assertEquals(20121L, (long) student.getLastUpdate());
    }
}