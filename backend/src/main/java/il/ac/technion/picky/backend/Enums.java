package il.ac.technion.picky.backend;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 *
 *  Enums in your backend are generated as String representations in the client code. In other words, if you are making use of the enum functionality in both the backend and client, you will need to define the enum in both places.
 *  So we define all api enums in this class so we could easily just copy this file to client
 *
 *  @see <a href="https://cloud.google.com/solutions/mobile/google-cloud-endpoints-for-android/#h.tb58w1ama1mo">About Supported Types</a>
 *
 */
public class Enums {

    // GENERAL is final grade of course
    public enum Moed {A("א", 4), B("ב", 2), C("ג", 1), GENERAL("כללי", 6);

        // hebname is used in client
        private static final Map<String, Moed> hebNameToMoed = new HashMap<>();

        static {
            for (Moed moed : values())
                hebNameToMoed.put(moed.hebName, moed);
        }

        private final String hebName;
        private final int weight;
        Moed(String hebName, int weight) {
            this.hebName = hebName;
            this.weight = weight;
        }

        public int weight(){ return weight; }

        public String hebName() {
            return hebName;
        }

        public static Moed fromHebName(String hebName) {
            return hebNameToMoed.get(hebName);
        }

        /**
         *
         * @return moeds in alphabetical order
         */
        public static String[] getHebNames() {
            return new TreeSet<>(hebNameToMoed.keySet()).toArray(new String[hebNameToMoed.size()]);
        }
    }

    public enum Source {
        CSExcel(false),
        EEExcel(false),
        HuExcel(false),
        PickyTeam(false),
        User(true),
        Other(true);
        private final boolean multiValuable;
        Source(final boolean multiValuable) { this.multiValuable = multiValuable; }
        public boolean isMultiValuable() { return multiValuable; }
    }
}
