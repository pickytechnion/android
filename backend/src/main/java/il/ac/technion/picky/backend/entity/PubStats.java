package il.ac.technion.picky.backend.entity;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;

import java.util.Map;

import static il.ac.technion.picky.backend.OfyService.ofy;



/**
 * Published grades that are approved by admins
 */
@Entity
@Cache
public class PubStats extends Stats {

	@SuppressWarnings("unused")
	private PubStats(){}
	
	public PubStats(Long courseId) {
		this.id = courseId;
	}

	public void recalculateData() {
		sum = 0;
		count = 0;
		Map<Long, PubSemStats> map = ofy().load().type(PubSemStats.class).ids(semesters);
		for (PubSemStats pubSemStats : map.values()) {
			sum += pubSemStats.getAverage();
			count++;
		}
		average = sum/(double)count;
	}
}