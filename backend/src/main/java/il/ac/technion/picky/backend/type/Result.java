package il.ac.technion.picky.backend.type;

/**
 * Return value for methods that don't return entities
 */
public class Result {

	public static final String EXISTS = "Already exists";
	public static final String NOT_EXISTS = "Not exists"; 
	
	private Boolean success;
	
	private String message = "";

	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void appendMessage(String msg) {
		message += msg;
	}
	
	public Result(){}
	
	public Result(boolean success) {
		this.success = success;
	}
	
	public Result(boolean success, String message) {
		this.success = success;
		this.message = message;
	}


}
