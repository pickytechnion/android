package il.ac.technion.picky.backend.type;

import java.util.AbstractMap;
import java.util.List;

/**
 * Custom return type because of limits in GAE:
 * You can return Map&lt;K, V&gt; but it will return Map&lt;String, Object&gt; so to avoid casting you may think to return List&lt;Map.Entry&lt;K,V&gt;&gt;
 * but return value can't be parametrized type so you can't. So you must implement class like this (but maybe there is better way).
 * <br>
 * @see
 *  <a href="https://cloud.google.com/appengine/docs/java/endpoints/paramreturn_types#return_types_and_request_body_types">endpoints return types</a>
 */
public class SemesterPubReport  {

    private AbstractMap.SimpleEntry<Long, List<PubReport>> entry;

    public SemesterPubReport() {

    }

    public SemesterPubReport(Long semester, List<PubReport> pubReports) {
        entry = new AbstractMap.SimpleEntry(semester, pubReports);
    }

    public Long getSemester() {
        return entry.getKey();
    }

    public List<PubReport> getPubReport() {
        return entry.getValue();
    }

}
