package il.ac.technion.picky.backend.entity;


import com.googlecode.objectify.annotation.Entity;

import java.util.ArrayList;
import java.util.List;

import il.ac.technion.picky.backend.Utils;

/**
 * Store UG grades for specific course and semester.
 * This store the actual grades and not just statistics like UgStats.
 * This is meant so that we won't lost all the grades that was shared by students and we could check that UgStats is correct.
 */
@Entity
public class UgSemStats extends Stats {

	@SuppressWarnings("unused")
	private UgSemStats(){}

	private List<Double> grades;
	
	public List<Double> inGetGrades() {
		return grades;
	}

	public UgSemStats(Long courseId, int year, int semester) {
		this.id = Utils.createId(courseId, year, semester);
		this.grades = new ArrayList<>();
	}

	@Override
	public void addGrade(double grade) {
		super.addGrade(grade);
		this.grades.add(grade);	
	}

	
	
	
}
