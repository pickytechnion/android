package il.ac.technion.picky.backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.Nullable;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.VoidWork;
import com.googlecode.objectify.Work;
import com.googlecode.objectify.cmd.LoadType;
import com.googlecode.objectify.cmd.Query;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import il.ac.technion.picky.backend.entity.PubReportEntity;
import il.ac.technion.picky.backend.entity.PubSemStats;
import il.ac.technion.picky.backend.entity.PubStats;
import il.ac.technion.picky.backend.entity.Stats;
import il.ac.technion.picky.backend.entity.Student;
import il.ac.technion.picky.backend.entity.UgSemStats;
import il.ac.technion.picky.backend.entity.UgStats;
import il.ac.technion.picky.backend.type.AppStats;
import il.ac.technion.picky.backend.type.CourseStats;
import il.ac.technion.picky.backend.type.PubReport;
import il.ac.technion.picky.backend.type.Result;
import il.ac.technion.picky.backend.type.SemesterPubReport;

import static il.ac.technion.picky.backend.Enums.Moed;
import static il.ac.technion.picky.backend.Enums.Source;
import static il.ac.technion.picky.backend.OfyService.ofy;

@Api(name = "picky", version = "v1",
        clientIds = {Constants.SERVICE_ACCOUNT_CLIENT_ID})

public class PickyAPI {

    private static final Logger LOGGER = Logger
            .getLogger(PickyAPI.class.getName());
    private static final String UNAUTHORIZED = "Unauthorized Access";

    @ApiMethod(name = "getStudent", httpMethod = HttpMethod.GET)
    public Student getStudent(@Named("id") String id, User user) throws UnauthorizedException {
        validateUser(user);
        return ofy().load().type(Student.class).id(id).now();
    }

    @ApiMethod(name = "deleteStudent", httpMethod = HttpMethod.DELETE)
    public Result deleteStudent(@Named("id") String id, User user) throws UnauthorizedException {
        validateUser(user);
        Student student = ofy().load().type(Student.class).id(id).now();
        if (student == null)
            return new Result(false, Result.NOT_EXISTS);
        ofy().delete().type(Student.class).id(id).now();
        return new Result(true);
    }

    /***********************************
     * UG
     ***********************************/

    @ApiMethod(name = "addUgGrade", httpMethod = HttpMethod.PUT)
    public Result addUgGrade(@Named("courseId") final Long courseId,
                             @Named("grade") final int grade, @Named("year") final int year,
                             @Named("semester") final int semester,
                             User user) throws UnauthorizedException {
        validateUser(user);
        addUgGradeAux(courseId, grade, year, semester);
        return new Result(true);
    }

    /**
     * @param id
     * @param grades json string e.g. [[2012,1,[[234114,65],[234114,75]]]]
     * @param user
     * @return
     * @throws UnauthorizedException
     * @throws JSONException
     */
    @ApiMethod(name = "addUgGrades", httpMethod = HttpMethod.PUT)
    public Result addUgGrades(@Named("id") String id,
                              @Named("grades") String grades,
                              User user) throws UnauthorizedException, JSONException {
        validateUser(user);
        Long lastUpdate = 0L;
        Long semesterId = 0L;
        Long updateLimit = getUpdateLimit();
        int gradesNum = 0;
        Student student = ofy().load().type(Student.class).id(id).now();
        if (student == null)
            student = new Student(id);
        else
            lastUpdate = student.getLastUpdate();

        JSONArray semesters = new JSONArray(grades);
        for (int i = 0; i < semesters.length(); i++) {
            JSONArray semester = semesters.getJSONArray(i);
            semesterId = Utils.createId(semester.getInt(0), semester.getInt(1));
            if (semesterId <= lastUpdate)
                continue;
            if (semesterId > updateLimit) {
                semesterId = updateLimit;
                break;
            }
            JSONArray courses = semester.getJSONArray(2);
            for (int j = 0; j < courses.length(); j++) {
                JSONArray course = courses.getJSONArray(j);
                addUgGradeAux((long) course.getInt(0), course.getInt(1),
                        semester.getInt(0), semester.getInt(1));
                gradesNum++;
            }
        }
        // update only if needed i.e. new grades were found
        if (gradesNum > 0) {
            student.setLastUpdate(semesterId);
            student.addGradesNumber(gradesNum);
            ofy().save().entity(student).now();
        }
        LOGGER.info(gradesNum + " grades added");
        return new Result(true, gradesNum + " grades added");
    }

    @ApiMethod(name = "getUgStats", httpMethod = HttpMethod.GET)
    public UgStats getUgStats(@Named("courseId") Long courseId, User user) throws UnauthorizedException {
        validateUser(user);
        return ofy().load().type(UgStats.class).id(courseId).now();
    }

    /**
     * @param courseId
     * @return actual grades from all semesters
     */
    @ApiMethod(name = "getUgGrades", httpMethod = HttpMethod.GET)
    List<Double> getUgGrades(@Named("courseId") Long courseId, User user) throws UnauthorizedException {
        validateUser(user);
        UgStats stats = getUgStats(courseId, user);
        return stats == null ? new ArrayList<Double>() : stats.inGetGrades();
    }

    @ApiMethod(name = "validateUgStats", httpMethod = HttpMethod.GET)
    public Result validateUgStats(@Named("courseId") Long courseId, User user) throws UnauthorizedException {
        validateUser(user);
        UgStats ugStats = getUgStats(courseId, user);
        List<Double> baseGrades = getUgGrades(courseId, user);
        if (ugStats == null && baseGrades.isEmpty())
            return new Result(true);
        if (ugStats == null)
            return new Result(false, "ugStats is null but grades list not empty");
        if (baseGrades.isEmpty())
            return new Result(false, "grades list is empty but ugStats not null");
        double sum = 0;
        int count = 0;
        for (double grade : baseGrades) {
            sum += grade;
            count++;
        }
        if (sum == ugStats.getSum() && count == ugStats.getCount())
            return new Result(true);
        return new Result(false, "ugStats: sum = " + ugStats.getSum() + " count = " + ugStats.getCount()
                + ", grades list: sum = " + sum + " count = " + count);
    }

    /***********************************
     * Published
     ***********************************/

    @ApiMethod(name = "addWaitingPubReport", httpMethod = HttpMethod.PUT)
    public Result addWaitingPubReport(@Named("source") Source source, @Named("courseId") Long courseId,
                                      @Named("grade") double grade, @Named("year") int year,
                                      @Named("semester") int semester, @Named("moed") Moed moed,
                                      @Nullable @Named("sourceString") String sourceString,
                                      @Nullable @Named("comments") String comments,
                                      User user) throws UnauthorizedException {
        validateUser(user);
        PubReportEntity pubReportEntity = new PubReportEntity(courseId, year, semester, new PubReport(source, moed, grade, sourceString,
                comments));
        ofy().save().entity(pubReportEntity).now();
        return new Result(true);
    }

    @ApiMethod(name = "getWaitingPubReports", httpMethod = HttpMethod.PUT)
    public List<PubReportEntity> getWaitingPubReports(
            User user, @Nullable @Named("limit") Integer limit) throws UnauthorizedException {
        validateUser(user);
        LoadType<PubReportEntity> reports = ofy().load().type(PubReportEntity.class);
        return limit == null ? reports.list() : reports.limit(limit).list();
    }

    @ApiMethod(name = "approvePubReport", httpMethod = HttpMethod.PUT)
    public Result approvePubReport(@Named("id") final Long id,
                                   User user) throws UnauthorizedException {
        validateUser(user);
        return ofy().transact(new Work<Result>() {
            public Result run() {
                PubReportEntity pubReportEntity = ofy().load().type(PubReportEntity.class).id(id).now();
                if (pubReportEntity == null)
                    return new Result(false, Result.NOT_EXISTS);
                Long courseId = pubReportEntity.getCourseId();
                Long courseSemId = Utils.createId(courseId, pubReportEntity.getYear(), pubReportEntity.getSemester());
                PubSemStats pubSemStats = ofy().load().type(PubSemStats.class).id(courseSemId).now();
                if (pubSemStats == null) {
                    pubSemStats = new PubSemStats(courseSemId);
                    LOGGER.info("created pubSemReports " + courseSemId);
                }
                pubSemStats.addApproved(pubReportEntity.getPubReport());

                PubStats pubStats = ofy().load().type(PubStats.class).id(courseId).now();
                if (pubStats == null) {
                    pubStats = new PubStats(courseId);
                    LOGGER.info("created pubStats " + courseId);
                }
                pubStats.addSemester(courseSemId);
                // before recalculateData we must first save pubSemStats
                ofy().save().entity(pubSemStats).now();
                pubStats.recalculateData();
                ofy().save().entity(pubStats).now();
                ofy().delete().type(PubReportEntity.class).id(id).now();
                LOGGER.info("updated pubStats " + courseId);
                return new Result(true);
            }
        });
    }

    @ApiMethod(name = "denyPubReport", httpMethod = HttpMethod.PUT)
    public Result denyPubReport(@Named("id") final Long id,
                                User user) throws UnauthorizedException {
        validateUser(user);
        return ofy().transact(new Work<Result>() {
            public Result run() {
                PubReportEntity pubReportEntity = ofy().load().type(PubReportEntity.class).id(id).now();
                if (pubReportEntity == null)
                    return new Result(false, Result.NOT_EXISTS);
                Long courseId = pubReportEntity.getCourseId();
                Long courseSemId = Utils.createId(courseId, pubReportEntity.getYear(), pubReportEntity.getSemester());
                PubSemStats pubSemStats = ofy().load().type(PubSemStats.class).id(courseSemId).now();
                if (pubSemStats == null) {
                    pubSemStats = new PubSemStats(courseSemId);
                    LOGGER.info("created pubSemReports " + courseSemId);
                }
                pubSemStats.addDenied(pubReportEntity.getPubReport());

                ofy().save().entity(pubSemStats).now();
                ofy().delete().type(PubReportEntity.class).id(id).now();
                return new Result(true);
            }
        });
    }

    @ApiMethod(name = "getPubStats", httpMethod = HttpMethod.GET)
    public PubStats getPubStats(@Named("courseId") Long courseId, User user) throws UnauthorizedException {
        validateUser(user);
        return ofy().load().type(PubStats.class).id(courseId).now();
    }

    @ApiMethod(name = "getApprovedPubReportsForSemester", httpMethod = HttpMethod.PUT)
    public List<PubReport> getApprovedPubReportsForSemester(@Named("courseId") final Long courseId,
                                                 @Named("year") final int year,
                                                 @Named("semester") final int semester,
                                                 User user) throws UnauthorizedException {
        validateUser(user);
        PubSemStats pubSemStats = ofy().load().type(PubSemStats.class).id( Utils.createId(courseId, year, semester)).now();
        return pubSemStats == null? null: pubSemStats.inGetApprovedPubReports();
    }

    @ApiMethod(name = "getApprovedPubReports", httpMethod = HttpMethod.PUT)
    public List<SemesterPubReport> getApprovedPubReports(@Named("courseId") final Long courseId,
                                                         User user) throws UnauthorizedException {
        validateUser(user);
        PubStats pubStats = ofy().load().type(PubStats.class).id(courseId).now();
        if (pubStats == null)
            return null;
        List<SemesterPubReport> result = new ArrayList<>();
        Map<Long, PubSemStats> sems = ofy().load().type(PubSemStats.class).ids(pubStats.inGetSemesters());
        for (Map.Entry<Long, PubSemStats> entry: sems.entrySet())
            result.add(new SemesterPubReport(Utils.extractSemId(entry.getKey()), entry.getValue().inGetApprovedPubReports()));
        return result;
    }

    /***********************************
     * UG and Published
     ***********************************/

    @ApiMethod(name = "getStats", httpMethod = HttpMethod.GET)
    public CourseStats getStats(@Named("courseId") Long courseId, User user) throws UnauthorizedException {
        validateUser(user);
        Key<UgStats> keyUg = Key.create(UgStats.class, courseId);
        Key<PubStats> keyPub = Key.create(PubStats.class, courseId);
        Map<Key<Stats>, Stats> map = ofy().load().keys(keyUg, keyPub);
        CourseStats stats = new CourseStats((UgStats) map.get(keyUg), (PubStats) map.get(keyPub));
        if (stats.getUgStats() == null && stats.getPubStats() == null)
            LOGGER.info(courseId + " has none");
        else
            LOGGER.info(courseId + " has" + (stats.getUgStats() == null? "" : " UG") + (stats.getPubStats() == null? "" : " Pub"));
        return stats;
    }

    /***********************************
     * admins methods
     ***********************************/

    @ApiMethod(name = "getAppStats", httpMethod = HttpMethod.GET)
    public AppStats getAppStats(User user) throws UnauthorizedException {
        validateUser(user);
        AppStats appStats = new AppStats();
        int studentsNum = 0;
        int ugGradesNum = 0;
        Query<Student> studentsQuery = ofy().load().type(Student.class);
        for (Student s : studentsQuery) {
            studentsNum++;
            ugGradesNum += s.getGradesNumber();
        }
        appStats.setStudentsNum(studentsNum);
        appStats.setUgGradesNum(ugGradesNum);

        Query<UgStats> ugStatsQuery = ofy().load().type(UgStats.class);
        Query<PubStats> pubStatsQuery = ofy().load().type(PubStats.class);
        Set<Long> coursesWithInfoSet = new HashSet<>();
        int num, count;
        double sum;
        sum = num = count = 0;
        for (UgStats stats : ugStatsQuery) {
            num++;
            sum += stats.getSum();
            count += stats.getCount();
            coursesWithInfoSet.add(stats.getId());
        }
        appStats.setCoursesWithUgInfoNum(num);
        if (count!=0)
            appStats.setUgGradesAvg(sum / count);
        sum = num = count = 0;
        for (PubStats stats : pubStatsQuery) {
            num++;
            sum += stats.getSum();
            count += stats.getCount();
            coursesWithInfoSet.add(stats.getId());
        }
        appStats.setCoursesWithPubInfoNum(num);
        if (count!=0)
            appStats.setPubGradesAvg(sum / count);

        appStats.setCoursesWithInfoNum(coursesWithInfoSet.size());

        return appStats;
    }

    /***********************************
     * private methods
     ***********************************/

    private void validateUser(User user) throws UnauthorizedException {
        if (user == null)
            throw new UnauthorizedException(UNAUTHORIZED);
    }

    /**
     * include the UpdateLimit when parsing. example: in 2015 1-5 all until
     * winter 2015 (excluding) in 2015 6-11 all until spring 2015 (excluding) in
     * 2015 12 all until summer 2015 (excluding)
     **/
    private Long getUpdateLimit() {
        Long year = (long) Calendar.getInstance().get(Calendar.YEAR);
        // month is 0-11
        int month = Calendar.getInstance().get(Calendar.MONTH);
        month++;
        // month = 1;
        if (month >= 1 && month <= 5)
            return (year - 1) * 10 + 3;
        else if (month >= 6 && month <= 11)
            return year * 10 + 1;
        else
            return (year) * 10 + 2;
    }

    private void addUgGradeAux(final Long courseId, final int grade, final int year, final int semester) {

        ofy().transact(new VoidWork() {

            @Override
            public void vrun() {
                Key<UgStats> key = Key.create(UgStats.class, courseId);
                Key<UgSemStats> keySem = Key.create(UgSemStats.class,
                        Utils.createId(courseId, year, semester));
                Map<Key<Stats>, Stats> map = ofy().load().keys(key, keySem);
                UgStats stats = (UgStats) map.get(key);
                UgSemStats statsSem = (UgSemStats) map.get(keySem);
                if (stats == null) {
                    stats = new UgStats(courseId);
                }
                if (statsSem == null) {
                    statsSem = new UgSemStats(courseId, year, semester);
                    stats.addSemester(keySem.getId());
                }
                stats.addGrade(grade);
                statsSem.addGrade(grade);
                ofy().save().entities(stats, statsSem).now();
            }
        });
    }

}
