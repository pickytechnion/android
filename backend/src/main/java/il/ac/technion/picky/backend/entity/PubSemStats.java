package il.ac.technion.picky.backend.entity;

import com.googlecode.objectify.annotation.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import il.ac.technion.picky.backend.type.PubReport;
import il.ac.technion.picky.backend.type.Result;

import static il.ac.technion.picky.backend.Enums.Moed;

/**
 * All PubReports and stats for specific course and semester.
 * New info go to waiting queue, and should be approved by admins.
 * The way to do it is using getToEvaluate and send it back to addApproved or addDenied.
 */

@Entity
public class PubSemStats extends Stats {

    private List<PubReport> approvedPubReports = new ArrayList<>();
    private List<PubReport> deniedPubReports = new ArrayList<>();

    public List<PubReport> inGetApprovedPubReports() {
        return approvedPubReports;
    }

    public PubSemStats() {

    }

    public PubSemStats(Long id) {
        this.id = id;
    }

    public Result addDenied(PubReport pubReport) {
        deniedPubReports.add(pubReport);
        return new Result(true);
    }

    public Result addApproved(PubReport pubReport) {
        if (pubReport.getSource().isMultiValuable()) {
            approvedPubReports.add(pubReport);
        } else {
            boolean foundMatch = false;
            for (PubReport existsPubReport : approvedPubReports) {
                if (existsPubReport.getSource().equals(pubReport.getSource()) && existsPubReport.getMoed().equals(pubReport.getMoed())) {
                    existsPubReport.setFinalAvg(pubReport.getFinalAvg());
                    foundMatch = true;
                }
            }
            if (!foundMatch) {
                approvedPubReports.add(pubReport);
            }
        }
        recalculateAverage();
        return new Result(true);
    }

    private void recalculateAverage() {
        Map<Moed, Stats> moedsStats = new HashMap<>();
        for (Moed moed : Moed.values())
            moedsStats.put(moed, new Stats());
        for (PubReport pubReport : approvedPubReports)
            moedsStats.get(pubReport.getMoed()).addGrade(pubReport.getFinalAvg());
        sum = 0;
        count = 0;
        for (Map.Entry<Moed, Stats> entry : moedsStats.entrySet()) {
            int weight = entry.getKey().weight();
            double moedAverage = entry.getValue().getAverage();
            if (moedAverage == 0)
                continue;
            sum += weight * moedAverage;
            count += weight;
        }
        average = sum / (double) count;
    }
}
