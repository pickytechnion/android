package il.ac.technion.picky.backend.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * This entity is created when user share his grade sheet.
 * It doesn't store any sensitive data.
 */
@Entity
public class Student {

	@Id
	private String id; // not real but hash

	private Long lastUpdate; // e.g. 20151 i.e. winter 2015
	
	private int gradesNumber; // how many grades he donate - for user rating

	public String getId() {
		return id;
	}

	public Long getLastUpdate() {
		return lastUpdate;
	}

	public int getGradesNumber() {
		return gradesNumber;
	}

	public void setLastUpdate(Long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public void addGradesNumber(int newGradesNumber) {
		gradesNumber+=newGradesNumber;
	}
	
	@SuppressWarnings("unused")
	private Student(){}
	
	public Student(String id, Long lastUpdate,
			int gradesNumber) {
		this.id = id;
		this.lastUpdate = lastUpdate;
		this.gradesNumber = gradesNumber;
	}
	
	public Student(String id) {
		this.id = id;
	}
	
}
