package il.ac.technion.picky.backend.entity;


import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.HashSet;
import java.util.Set;

/**
 * Base class for UgStats and PubStats that store statistics for specific course
 */
@Entity
public class Stats {
	
	@Id
	protected Long id;
	
	protected double sum;
	protected int count;

	//UgSemStats or PubSemStats ids. Don't make this (or any Entity's field) final
	protected Set<Long> semesters = new HashSet<>();

	@Index
	protected double average=0;

	public double getSum() { return sum; }

	public Long getId() { return id; }

	public double getAverage() {
		return average;
	}

	public int getCount() {
		return count;
	}

	public void addSemester(Long semester) {
		this.semesters.add(semester);
	}

	public Set<Long> inGetSemesters() {
		return semesters;
	}

	public void addGrade(double grade){
		sum += grade;
		count++;
		average = sum / count;
	}
}
