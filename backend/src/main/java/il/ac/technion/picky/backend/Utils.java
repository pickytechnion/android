package il.ac.technion.picky.backend;

public class Utils {

	public static Long createId(Long courseId, int year, int semester){
		return courseId * 100000 + year *10 + semester;
	}

	public static Long createId(int year, int semester){
		return (long) (year * 10 + semester);
	}

	public static Long extractSemId(Long courseSemId){
		return courseSemId % 100000;
	}
}
