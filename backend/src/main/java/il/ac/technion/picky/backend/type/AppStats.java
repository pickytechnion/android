package il.ac.technion.picky.backend.type;

/**
 * Created by ohad on 8/7/15.
 * Returns app statistics
 */
public class AppStats {
    private int studentsNum;
    private int ugGradesNum;
    private int coursesWithUgInfoNum;
    private int coursesWithPubInfoNum;
    private int coursesWithInfoNum;
    private double ugGradesAvg;
    private double pubGradesAvg;

    public AppStats(int studentsNum, int ugGradesNum, int coursesWithUgInfoNum, int coursesWithPubInfoNum, int coursesWithInfoNum, double ugGradesAvg, double pubGradesAvg) {
        this.studentsNum = studentsNum;
        this.ugGradesNum = ugGradesNum;
        this.coursesWithUgInfoNum = coursesWithUgInfoNum;
        this.coursesWithPubInfoNum = coursesWithPubInfoNum;
        this.coursesWithInfoNum = coursesWithInfoNum;
        this.ugGradesAvg = ugGradesAvg;
        this.pubGradesAvg = pubGradesAvg;
    }

    public AppStats() {

    }

    public void setStudentsNum(int studentsNum) {
        this.studentsNum = studentsNum;
    }

    public void setUgGradesNum(int ugGradesNum) {
        this.ugGradesNum = ugGradesNum;
    }

    public void setCoursesWithUgInfoNum(int coursesWithUgInfoNum) {
        this.coursesWithUgInfoNum = coursesWithUgInfoNum;
    }

    public void setCoursesWithPubInfoNum(int coursesWithPubInfoNum) {
        this.coursesWithPubInfoNum = coursesWithPubInfoNum;
    }

    public void setCoursesWithInfoNum(int coursesWithInfoNum) {
        this.coursesWithInfoNum = coursesWithInfoNum;
    }

    public void setUgGradesAvg(double ugGradesAvg) {
        this.ugGradesAvg = ugGradesAvg;
    }

    public void setPubGradesAvg(double pubGradesAvg) {
        this.pubGradesAvg = pubGradesAvg;
    }

    public int getStudentsNum() {
        return studentsNum;
    }

    public int getUgGradesNum() {
        return ugGradesNum;
    }

    public int getCoursesWithUgInfoNum() {
        return coursesWithUgInfoNum;
    }

    public int getCoursesWithPubInfoNum() {
        return coursesWithPubInfoNum;
    }

    public int getCoursesWithInfoNum() {
        return coursesWithInfoNum;
    }

    public double getUgGradesAvg() {
        return ugGradesAvg;
    }

    public double getPubGradesAvg() {
        return pubGradesAvg;
    }

    public String getString() {
        return  "Number of students is: " + studentsNum + "\n"
                + "Number of ug grades is: " + ugGradesNum + "\n"
                + "Number of courses with ug info is: " + coursesWithUgInfoNum + "\n"
                + "Number of courses with pub info is: " + coursesWithPubInfoNum + "\n"
                + "Number of courses with info is: " + coursesWithInfoNum + "\n"
                + "Average of ug courses is: " + ugGradesAvg + "\n"
                + "Average of pub courses is: " + pubGradesAvg + "\n";
    }
}