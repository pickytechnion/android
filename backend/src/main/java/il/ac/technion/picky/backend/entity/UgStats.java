package il.ac.technion.picky.backend.entity;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static il.ac.technion.picky.backend.OfyService.ofy;

/**
 * Grades from based on grade sheets of students
 */
@Entity
@Cache
public class UgStats extends Stats {

	@SuppressWarnings("unused")
	private UgStats(){}
	
	public UgStats(Long courseId) {
		this.id = courseId;
	}

	public List<Double> inGetGrades() {
		List<Double> grades = new ArrayList<>();
		Map<Long, UgSemStats> map = ofy().load().type(UgSemStats.class).ids(semesters);
		for (UgSemStats semStats : map.values())
			grades.addAll(semStats.inGetGrades());
		return grades;
    }
}
