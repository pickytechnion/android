package il.ac.technion.picky.backend.type;

import com.google.api.server.spi.config.AnnotationBoolean;
import com.google.api.server.spi.config.ApiResourceProperty;

import static il.ac.technion.picky.backend.Enums.Moed;
import static il.ac.technion.picky.backend.Enums.Source;

/**
 * EmbeddedEntity for storing report info.
 * @see <a href="https://github.com/objectify/objectify/wiki/Entities#embedded-classes">embedded classes</a>
 *
 */
public class PubReport {

	private Source source;
	private Moed moed;
	private double finalAvg;
	private String sourceString;
	private String comments;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PubReport pubReport = (PubReport) o;

		if (Double.compare(pubReport.getFinalAvg(), getFinalAvg()) != 0) return false;
		if (getSource() != pubReport.getSource()) return false;
		if (getMoed() != pubReport.getMoed()) return false;
		if (getSourceString() != null ? !getSourceString().equals(pubReport.getSourceString()) : pubReport.getSourceString() != null)
			return false;
		return !(getComments() != null ? !getComments().equals(pubReport.getComments()) : pubReport.getComments() != null);
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = getSource().hashCode();
		result = 31 * result + getMoed().hashCode();
		temp = Double.doubleToLongBits(getFinalAvg());
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (getSourceString() != null ? getSourceString().hashCode() : 0);
		result = 31 * result + (getComments() != null ? getComments().hashCode() : 0);
		return result;
	}

	public PubReport(Source source, Moed moed, double finalAvg, String sourceString, String comments) {
		this.source = source;
		this.moed = moed;
		this.finalAvg = finalAvg;
		this.sourceString = sourceString;
		this.comments = comments;
	}

	public PubReport(Source source, Moed moed, double finalAvg) {
		this.source = source;
		this.moed = moed;
		this.finalAvg = finalAvg;
	}

	public PubReport() {}
	
	public Moed getMoed() {
		return moed;
	}

	public Source getSource() {
		return source;
	}

	public String getSourceString() {	return sourceString;	}

	public void setSourceString(String sourceString) {	this.sourceString = sourceString; }

	public double getFinalAvg() {
		return finalAvg;
	}

	public void setFinalAvg(double finalAvg) {
		this.finalAvg = finalAvg;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	public String getString() {
		return 	"source=" + source.name() +
				", moed=" + moed.name() +
				", finalAvg=" + finalAvg +
				", sourceString='" + sourceString + '\'' +
				", comments='" + comments;
	}
}
