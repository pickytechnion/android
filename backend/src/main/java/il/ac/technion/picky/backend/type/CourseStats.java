package il.ac.technion.picky.backend.type;

import il.ac.technion.picky.backend.entity.PubStats;
import il.ac.technion.picky.backend.entity.UgStats;

/**
 * Wrapper to return UgStats and PubStats in one API request
 */
public class CourseStats {

	private UgStats ug;
	private PubStats pub;
	
	public UgStats getUgStats() {
		return ug;
	}
	
	public PubStats getPubStats() {
		return pub;
	}
	
	public boolean hasUg(){
		return ug!=null;	
	}
	
	public boolean hasPub(){
		return pub!=null;	
	}
	
	public CourseStats(){}
	
	public CourseStats(UgStats ug, PubStats pub) {
		this.ug = ug;
		this.pub = pub;
	}
	
	
}
