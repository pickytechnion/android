package il.ac.technion.picky.backend;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

import il.ac.technion.picky.backend.entity.PubReportEntity;
import il.ac.technion.picky.backend.entity.PubSemStats;
import il.ac.technion.picky.backend.entity.PubStats;
import il.ac.technion.picky.backend.entity.Stats;
import il.ac.technion.picky.backend.entity.Student;
import il.ac.technion.picky.backend.entity.UgSemStats;
import il.ac.technion.picky.backend.entity.UgStats;

/**
 * Objectify service that acts as a wrapper to operations on the app engine datastore
 */
public class OfyService {
	static {
        factory().register(Student.class);
        factory().register(Stats.class);
        factory().register(UgStats.class);
        factory().register(UgSemStats.class);
        factory().register(PubStats.class);
        factory().register(PubSemStats.class);
        factory().register(PubReportEntity.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    private static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }

}
