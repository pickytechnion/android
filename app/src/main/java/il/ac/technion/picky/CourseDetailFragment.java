package il.ac.technion.picky;

import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.appspot.pickybackend.picky.model.CourseStats;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import il.ac.technion.picky.backend.ApiService;
import il.ac.technion.picky.backend.GetAverageAsyncTask;
import il.ac.technion.picky.backend.PubReportWrapper;
import il.ac.technion.picky.backend.PutPublishedAverageTask;

import static il.ac.technion.picky.backend.Enums.Moed;

/**
 * A fragment representing a single Course detail screen.
 * This fragment is either contained in a {@link CourseListActivity}
 * in two-pane mode (on tablets) or a {@link CourseDetailActivity}
 * on handsets.
 */
public class CourseDetailFragment extends Fragment {

    public static final String ARG_ID = "id";
    public static final String ARG_NUMBER = "number";
    public static final String ARG_NAME = "name";

    private static final String UG_BASE_LINK = "https://ug3.technion.ac.il/rishum/course/";
    private static final String GRADUATE_BASE_LINK = "http://www.graduate.technion.ac.il/heb/subjects/?SUB=";
    private static final DecimalFormat GRADE_FORMAT = new DecimalFormat("#.#");

    private ValueAnimator animator;
    private RatingBar ratingView;
    private ImageView sourceView;
    private ImageView swapIcon;
    private TextView gradeView;
    private FloatingActionButton plusMenuButton;
    private TextView textViewSyllabus;
    private CourseStats stats;
    // use to toggle between ug and pub if both exists
    private boolean isUgDisplayed;
    private long courseId;

    private AsyncTask syllabusAsyncTask;
    private AsyncTask averageAsyncTask;

    private static class StatsWrapper implements Serializable {

        // without static keyword there is RuntimeException (Caused by: NotSerializableException: CourseStats) that happens only when opening mail intent with FloatingActionButton
        private static CourseStats stats;

        public CourseStats getStats() {
            return stats;
        }

        public StatsWrapper(CourseStats stats) {
            this.stats = stats;
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("syllabus", textViewSyllabus.getText().toString());
        outState.putBoolean("isUgDisplayed", isUgDisplayed);
        outState.putSerializable("stats", new StatsWrapper(stats));
    }

    @Override
    public void onStop() {
        super.onStop();
        // Cancel AsyncTasks if user exit fragment before they were finished
        if (syllabusAsyncTask != null && syllabusAsyncTask.getStatus() != AsyncTask.Status.FINISHED)
            syllabusAsyncTask.cancel(true);
        if (averageAsyncTask != null && averageAsyncTask.getStatus() != AsyncTask.Status.FINISHED)
            averageAsyncTask.cancel(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_course_detail, container, false);

        FloatingActionButton reportButton = (FloatingActionButton) rootView.findViewById(R.id.button_report);
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arguments = getArguments();
                if (arguments == null)
                    return;
                String[] addresses = {"pickytechnion@gmail.com"};
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, addresses);
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_report_subject) + " "
                        + arguments.getString(ARG_NAME) + " " + arguments.getString(ARG_NUMBER));
                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.email_report_content));
                if (intent.resolveActivity(getActivity().getPackageManager()) != null)
                    startActivity(intent);
                FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) getActivity().findViewById(R.id.plus_menu);
                floatingActionsMenu.collapse();
            }
        });
        FloatingActionButton addPublishedButton = (FloatingActionButton) rootView.findViewById(R.id.button_addPublished);
        addPublishedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddPublishedDialog dialog = new AddPublishedDialog();
                Bundle bundle = new Bundle();
                bundle.putLong(ARG_ID, courseId);
                dialog.setArguments(bundle);
                dialog.show(getFragmentManager(), null);
                FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) getActivity().findViewById(R.id.plus_menu);
                floatingActionsMenu.collapse();
            }
        });
        ratingView = (RatingBar) rootView.findViewById(R.id.reliabilityBar);
        sourceView = (ImageView) rootView.findViewById(R.id.course_average_source);
        gradeView = (TextView) rootView.findViewById(R.id.course_average);
        textViewSyllabus = (TextView) rootView.findViewById(R.id.course_syllabus);
        swapIcon = (ImageView) rootView.findViewById(R.id.course_swap_icon);
        plusMenuButton = (FloatingActionButton) rootView.findViewById(R.id.button_report);
        // Get & Show the info of the course from the intent
        // first run in two-pane (tablet) it is null so show empty fragment
        Bundle arguments = getArguments();
        if (savedInstanceState != null) {
            textViewSyllabus.setText(savedInstanceState.getString("syllabus"));
            isUgDisplayed = savedInstanceState.getBoolean("isUgDisplayed");
            stats = ((StatsWrapper) savedInstanceState.getSerializable("stats")).getStats();
            displayStats(false);
        } else if (arguments != null) {
            courseId = arguments.getLong(ARG_ID);
            syllabusAsyncTask = new GetSyllabusAsyncTask().execute(arguments.getString(ARG_NUMBER));
            initAnimator();
            averageAsyncTask = new GetAverageAsyncTask().setOnPostExecuteListener(onPostExecuteListener).execute(courseId);
            animator.start();
        }
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_guide:
                if (((PickyApplication) getActivity().getApplication()).checkAndUpdateShowcaseTimes())
                    new ShowcaseViewDisplayer().display();
                else
                    TealSnackbar.show(getActivity().findViewById(R.id.course_detail_container), getString(R.string.showcase_limit));
                return true;
            case android.R.id.home:
                // this command destroys parent activity
                //NavUtils.navigateUpTo(getActivity(), new Intent(getActivity(), CourseListActivity.class));
                if (getActivity() instanceof CourseDetailActivity)
                    getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private final GetAverageAsyncTask.OnPostExecuteListener onPostExecuteListener = new GetAverageAsyncTask.OnPostExecuteListener() {
        @Override
        public void onPostExecute(CourseStats stats, Throwable throwable) {
            animator.end();
            // activity was destroyed e.g. user pressed back button before the asynctask ended
            if (getActivity() == null)
                return;
            if (throwable == null) {
                CourseDetailFragment.this.stats = stats;
                if (stats == null || (stats.getUgStats() == null && stats.getPubStats() == null)) {
                    gradeView.setText(R.string.unknown_average);
                    TealSnackbar.show(getActivity().findViewById(R.id.details_scroll_view), getString(stats == null ? R.string.exception_general : R.string.no_course_details));
                    return;
                }
                displayStats(true);
            } else {
                gradeView.setText(R.string.unknown_average);
                String message;
                if (throwable.getClass().equals(GoogleJsonResponseException.class)) {
                    if (ApiService.isOverQuotaException((GoogleJsonResponseException) throwable))
                        message = getString(R.string.backend_over_quota);
                    else
                        message = getString(R.string.backend_error);
                } else if (throwable.getClass().equals(IOException.class))
                    message = getString(R.string.network_problem);
                else
                    message = getString(R.string.exception_general);
                TealSnackbar.show(CourseDetailFragment.this.getActivity().findViewById(R.id.details_scroll_view), message);
            }
        }
    };

    private void displayStats(boolean isFirstDisplay) {
        if (stats.getUgStats() != null && stats.getPubStats() != null) {
            swapIcon.setVisibility(View.VISIBLE);
            if (isFirstDisplay) {
                Animation alphaAnimation = new AlphaAnimation(0.0f, 0.2f);
                alphaAnimation.setDuration(1000);
                swapIcon.startAnimation(alphaAnimation);
            }
            // display the more reliable grade or by isUgDisplayed if not the first display
            if ((isFirstDisplay && getPublishedRating(stats.getPubStats().getCount()) > getUgRating(stats.getUgStats().getCount())) || (!isFirstDisplay && !isUgDisplayed)) {
                isUgDisplayed = false;
                displayPubStats();
            } else {
                isUgDisplayed = true;
                displayUgStats();
            }

            gradeView.setOnTouchListener(new View.OnTouchListener() {
                private float x1, x2;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            x1 = event.getX();
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            x2 = event.getX();
                            if (x1 < x2 || x2 < x1) {
                                if (isUgDisplayed)
                                    displayPubStats();
                                else
                                    displayUgStats();
                                isUgDisplayed = !isUgDisplayed;
                            }
                        }
                    }
                    return true;
                }
            });
        } else if (stats.getUgStats() != null)
            displayUgStats();
        else if (stats.getPubStats() != null)
            displayPubStats();
        else
            gradeView.setText(R.string.unknown_average);
        if (isFirstDisplay)
            gradeView.animate().rotation(360f).setDuration(1000);
    }

    private void displayUgStats() {
        gradeView.setText(GRADE_FORMAT.format(stats.getUgStats().getAverage()));
        ratingView.setRating((float) getUgRating(stats.getUgStats().getCount()));
        sourceView.setImageResource(R.drawable.ic_people);
    }

    private void displayPubStats() {
        gradeView.setText(GRADE_FORMAT.format(stats.getPubStats().getAverage()));
        ratingView.setRating((float) getPublishedRating(stats.getPubStats().getCount()));
        sourceView.setImageResource(R.drawable.ic_megaphone_white);
        sourceView.setScaleX((float) 1.5);
        sourceView.setScaleY((float) 1.3);
    }

    private void initAnimator() {
        animator = new ValueAnimator();
        animator.setObjectValues(0, 100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                gradeView.setText(Double.toString(Double.parseDouble(String.valueOf(animation.getAnimatedValue())) / 10));
            }
        });
        animator.setEvaluator(new TypeEvaluator<Integer>() {
            public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
                return Math.round((endValue - startValue) * fraction * 10);
            }
        });
        animator.setDuration(2000);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setRepeatMode(ValueAnimator.REVERSE);
    }

    private static double getUgRating(Integer count) {
        if (count < 5)
            return 0.5;
        if (count < 10)
            return 1;
        if (count < 20)
            return 1.5;
        if (count < 30)
            return 2;
        if (count < 50)
            return 2.5;
        return 3;
    }

    private static double getPublishedRating(Integer count) {
        return Math.min(2.0 + ((double) count) / 10.0, 3.0);
    }

    private class ShowcaseViewDisplayer {

        private ScrollView scrollView;
        private boolean showcaseActive;

        public void display() {
            displayShowcaseViewOne();
        }

        public boolean isShowcaseActive() {
            return showcaseActive;
        }

        private void displayShowcaseViewOne() {
            scrollView = (ScrollView) getActivity().findViewById(R.id.details_scroll_view);
            scrollView.pageScroll(View.FOCUS_UP);

            ShowcaseView showcaseView = new ShowcaseView.Builder(getActivity())
                    .setContentTitle(R.string.showcase_source_title)
                    .setContentText(R.string.showcase_source_text)
                    .setTarget(new ViewTarget(sourceView))
                    .hideOnTouchOutside()
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {

                        @Override
                        public void onShowcaseViewHide(com.github.amlcurran.showcaseview.ShowcaseView showcaseView) {
                            showcaseView.setVisibility(View.GONE);
                            showOverlayTutorialTwo();
                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView) {
                            showcaseActive = true;
                        }

                    })
                    .build();
            showcaseView.hideButton();
        }

        private void showOverlayTutorialTwo() {
            ShowcaseView showcaseView = new ShowcaseView.Builder(getActivity())
                    .setContentTitle(R.string.showcase_reliability_title)
                    .setContentText(R.string.showcase_reliability_text)
                    .setTarget(new ViewTarget(ratingView))
                    .hideOnTouchOutside()
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {

                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView) {
                            showcaseView.setVisibility(View.GONE);
                            showOverlayTutorialThree();
                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView) {

                        }
                    })
                    .build();
            showcaseView.hideButton();
        }

        private void showOverlayTutorialThree() {
            scrollView.scrollTo(0, gradeView.getTop());
            ShowcaseView showcaseView = new ShowcaseView.Builder(getActivity())
                    .setContentTitle(R.string.showcase_grade_title)
                    .setContentText(R.string.showcase_grade_text)
                    .setTarget(new ViewTarget(gradeView))
                    .hideOnTouchOutside()
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {

                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView) {
                            showcaseView.setVisibility(View.GONE);
                            showOverlayTutorialFour();
                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView) {

                        }
                    })
                    .build();
            showcaseView.hideButton();
        }

        private void showOverlayTutorialFour() {
            scrollView.pageScroll(View.FOCUS_UP);
            ShowcaseView showcaseView = new ShowcaseView.Builder(getActivity())
                    .setContentTitle(R.string.showcase_add_grade_title)
                    .setContentText(R.string.showcase_add_grade_text)
                    .setTarget(new ViewTarget(plusMenuButton))
                    .hideOnTouchOutside()
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {

                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView) {
                            showcaseView.setVisibility(View.GONE);
                            showcaseActive = false;
                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView) {

                        }
                    })
                    .build();
            showcaseView.hideButton();
        }
    }

    private class GetSyllabusAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                return Jsoup.connect(GRADUATE_BASE_LINK + params[0]).get().body().getElementsByClass("syl").text();
            } catch (IOException e) {
                e.printStackTrace();
                return getString(R.string.error_syllabus);
            }
        }

        @Override
        protected void onPostExecute(String s) {
            textViewSyllabus.setText(s);
        }
    }

    public static class AddPublishedDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new AlertDialog.Builder(getActivity()).setView(View.inflate(getActivity(), R.layout.add_published_grade, null))
                    .setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            NumberPicker semesterPicker = (NumberPicker) getDialog().findViewById(R.id.numberPicker_semester);
                            NumberPicker yearPicker = (NumberPicker) getDialog().findViewById(R.id.numberPicker_year);
                            NumberPicker moedPicker = (NumberPicker) getDialog().findViewById(R.id.numberPicker_moed);
                            NumberPicker averagePicker = (NumberPicker) getDialog().findViewById(R.id.numberPicker_average);
                            EditText sourceEditText = (EditText) getDialog().findViewById(R.id.add_published_source);
                            EditText notesEditText = (EditText) getDialog().findViewById(R.id.add_published_notes);

                            PubReportWrapper pubReportWrapper = new PubReportWrapper(
                                    getArguments().getLong(ARG_ID),
                                    Integer.valueOf(yearPicker.getDisplayedValues()[yearPicker.getValue()]),
                                    semesterPicker.getValue() + 1,
                                    Double.valueOf(averagePicker.getDisplayedValues()[averagePicker.getValue()]),
                                    Moed.fromHebName((moedPicker.getDisplayedValues()[moedPicker.getValue()])).toString(),
                                    sourceEditText.getText().toString(),
                                    notesEditText.getText().toString()
                            );
                            new PutPublishedAverageTask(getActivity()).execute(pubReportWrapper);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            AddPublishedDialog.this.getDialog().cancel();
                        }
                    }).create();
        }

        @Override
        public void onStart() {
            super.onStart();
            Calendar calendar = Calendar.getInstance();
            NumberPicker yearPicker = (NumberPicker) getDialog().findViewById(R.id.numberPicker_year);
            List<String> years = new ArrayList<>();
            int yearToday = calendar.get(Calendar.YEAR);
            for (int year = 2000; year <= yearToday; year++)
                years.add(String.valueOf(year));
            yearPicker.setMinValue(0);
            yearPicker.setMaxValue(years.size() - 1);
            yearPicker.setValue(years.size() - 1);
            yearPicker.setDisplayedValues(years.toArray(new String[years.size()]));

            NumberPicker semesterPicker = (NumberPicker) getDialog().findViewById(R.id.numberPicker_semester);
            String[] semesters = new String[]{"חורף", "אביב", "קיץ"};
            semesterPicker.setMinValue(0);
            semesterPicker.setMaxValue(semesters.length - 1);
            semesterPicker.setValue(calendar.get(Calendar.MONTH) <= 6 ? 0 : 1);
            semesterPicker.setDisplayedValues(semesters);

            NumberPicker moedPicker = (NumberPicker) getDialog().findViewById(R.id.numberPicker_moed);
            String[] moeds = Moed.getHebNames();
            moedPicker.setMinValue(0);
            moedPicker.setMaxValue(moeds.length - 1);
            moedPicker.setDisplayedValues(moeds);

            NumberPicker averagePicker = (NumberPicker) getDialog().findViewById(R.id.numberPicker_average);
            List<String> averages = new ArrayList<>();
            for (int i = 0; i <= 1000; i++)
                averages.add(String.valueOf(GRADE_FORMAT.format(((float) i) / 10.0)));
            averagePicker.setMinValue(0);
            averagePicker.setMaxValue(averages.size() - 1);
            averagePicker.setValue(700);
            averagePicker.setDisplayedValues(averages.toArray(new String[averages.size()]));
        }
    }

}

