package il.ac.technion.picky.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Filter implements Serializable {

    public enum FilterKey {
        SEARCH_VIEW_NUMBER("number LIKE ?"),
        SEARCH_VIEW_NAME("name LIKE ?"),
        FACULTY("faculty=?"),
        POINTS("points=?"),
        SEMESTER("year=? AND semester=?"),
        HAS_EXAM("hasExam=?"),
        EXAMS_DATES_RANGE_OR_NO_EXAM("((examAISO BETWEEN ? AND ?) OR (examBISO BETWEEN ? AND ?)  OR hasExam=0)"),
        /******
         * this filters cannot be used together
         ******/
        PROJECT("(name LIKE 'פרוייקט%' OR name LIKE 'פרויקט%')"),
        SEMINAR("name LIKE '%סמינר%'"),
        LAB("(name LIKE 'מעבדה%' OR name LIKE 'מעבדת%' OR name LIKE 'מעב''%' OR name LIKE 'מע.%')"),
        SPORT("_id >= 394800 AND _id < 395000");
        /***************************************************/
        private final String condition;

        public String condition() {
            return condition;
        }

        FilterKey(String condition) {
            this.condition = condition;
        }
    }

    private static final String AND = " AND ";
    private final Map<FilterKey, List<String>> args;
    private String selection;
    private String[] selectionArgs;

    public String getSelection() {
        return selection;
    }

    public String[] getSelectionArgs() {
        return selectionArgs;
    }

    public List<String> getArgs(FilterKey key) {
        return args.get(key);
    }

    /**
     * Returns only first arg
     * @param key
     * @return
     */
    public String getArg(FilterKey key) {
        List<String> l = args.get(key);
        return (l==null || l.size()==0) ? null : l.get(0);
    }

    public Filter() {
        // LinkedHashMap for predictable iteration order for matching conditions to parameters
        args = new LinkedHashMap<>();
    }

    public void add(FilterKey key, String... parameters) {
        if (parameters.length == 0) {
            args.put(key, Collections.<String>emptyList());
            updateSelection();
            return;
        }
        if (args.containsKey(key)) {
            List<String> l = args.get(key);
            l.clear();
            l.addAll(Arrays.asList(parameters));
        } else {
            args.put(key, new ArrayList<>(Arrays.asList(parameters)));
            updateSelection();
        }
        updateSelectionArgs();
    }

    public void remove(FilterKey key) {
        if (!args.containsKey(key))
            return;
        args.remove(key);
        updateSelection();
        updateSelectionArgs();
    }

    public void clear() {
        if (args.isEmpty())
            return;
        args.clear();
        selection = null;
        selectionArgs = null;
    }

    /**
     * clear all filters but exclude some
     *
     * @param exclude filters not for remove
     */
    public void clear(FilterKey... exclude) {
        if (args.isEmpty())
            return;
        if (exclude.length == 0) {
            clear();
            return;
        }
        List<FilterKey> excludeList = Arrays.asList(exclude);
        Iterator<FilterKey> iter = args.keySet().iterator();
        while (iter.hasNext()) {
            FilterKey key = iter.next();
            if (!excludeList.contains(key))
                iter.remove();
        }
        updateSelection();
        updateSelectionArgs();
    }

    private void updateSelection() {
        StringBuilder sb = new StringBuilder();
        for (FilterKey key : args.keySet())
            sb.append(key.condition()).append(AND);
        if (sb.length() == 0) {
            selection = null;
            return;
        }
        sb.setLength(sb.length() - AND.length());
        selection = sb.toString();
    }

    private void updateSelectionArgs() {
        List<String> argsList = new ArrayList<>();
        for (List<String> l : args.values())
            argsList.addAll(l);
        selectionArgs = argsList.isEmpty() ? null : argsList.toArray(new String[argsList.size()]);
    }

    @Override
    public String toString() {
        return selection;
    }
}
