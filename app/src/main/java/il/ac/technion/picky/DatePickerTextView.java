package il.ac.technion.picky;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Custom TextView that opens DatePicker when you click it and displays the date in format dd/MM/yyyy e.g. 31/12/2015
 */
public class DatePickerTextView extends TextView implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private static final String DATE_FORMAT_STRING = "%02d/%02d/%d";
    /** Offset to fix this problem: In Android 5.1 you cannot choose max date because apparently it depends on the time.
     * For example, if max date is 10/11/15 6:30 PM and the current time is 8:31 PM (Israel is GMT+2) you cannot choose max date
     * but if max date is 8:32 PM you can.
     */
    private static final long DATE_OFFSET = TimeUnit.DAYS.toMillis(1)-1;

    private DatePickerDialog datePickerDialog;

    public DatePicker getDatePicker() {
        return datePickerDialog.getDatePicker();
    }

    public long getMinDate() {
        return getDatePicker().getMinDate();
    }

    public DatePickerTextView setMinDate(long minDate) {
        getDatePicker().setMinDate(minDate);
        return this;
    }

    public DatePickerTextView setMinDate(String minDate) {
        Date date = parse(minDate);
        return setMinDate(date == null ? 0 : date.getTime());
    }

    public long getMaxDate() {
        return getDatePicker().getMaxDate();
    }

    public DatePickerTextView setMaxDate(long maxDate) {
        getDatePicker().setMaxDate(maxDate  + DATE_OFFSET);
        return this;
    }

    public DatePickerTextView setMaxDate(String maxDate) {
        Date date = parse(maxDate);
        return setMaxDate(date == null ? Long.MAX_VALUE : date.getTime());
    }

    public DatePickerTextView(Context context) {
        super(context);
        init();
    }

    public DatePickerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DatePickerTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DatePickerTextView setInitialDate(long date) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(date));
        datePickerDialog.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        return this;
    }

    public DatePickerTextView setInitialDate(String date) {
        return setInitialDate(parse(date).getTime());
    }

    @Override
    public void onClick(View view) {
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        setText(String.format(DATE_FORMAT_STRING, day, month + 1, year));
    }

    /**
     * @param date e.g. 31/12/2014
     * @return e.g. 2014-12-31
     */
    public static String toISODate(String date) {
        String[] a = date.split("/");
        if (a.length < 3)
            return "";
        return a[2] + "-" + a[1] + "-" + a[0];
    }

    /**
     * @param date e.g. 2014-12-31
     * @return e.g. 31/12/2014
     */
    public static String fromISODate(String date) {
        String[] a = date.split("-");
        if (a.length < 3)
            return "";
        return a[2] + "/" + a[1] + "/" + a[0];
    }

    /**
     * @param date e.g. 31/12/2014
     * @return
     */
    public static Date parse(String date) {
        try {
            return DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }


    /**
     * Create new DatePickerDialog with TextView text as initial date.
     * For preventing errors use this before you using one of this class date related setters e.g. setMinMax
     *
     * @return
     */
    public DatePickerTextView resetDatePicker() {
        Date d = parse(getText().toString());
        if (d == null)
            d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        datePickerDialog = new DatePickerDialog(getContext(), this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        getDatePicker().setSpinnersShown(false);
        getDatePicker().setCalendarViewShown(true);
        return this;
    }

    private void init() {
        setOnClickListener(this);
        resetDatePicker();
    }

}
