package il.ac.technion.picky.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Bidi;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipFile;

/**
 * Parses REP file which contains details of courses given in specific semester.
 * Parses only part of the data (e.g. not schedule). Can save parsed data to
 * file in JSON format. <br>
 *
 * <br><b>Comments:</b><br> <b>1.</b> faculty number taken from 2 first numbers
 * of first course of the faculty (There are exceptional courses e.g. 197008 (in
 * "Applied Mathematics" syllabus) in mathematics which usually has "10" prefix)
 * <br><b>2.</b> By default parses only undergraduate courses (see rules in
 * isUgCourse method) and parses only until Biomedical Engineering + sport i.e.
 * skip graduate programs e.g. Polymer Engineering, Energy, Nanoscience &
 * Nanotechnology . You can change this by setting isUgOnly to false. <br>
 * <b>3.</b> By default convert exams dates to ISO format. You can change this
 * by setting isoDates to false.
 */
public class RepParser {
    public static final String REP_ZIP_FILE_NAME = "REPFILE.zip";
    private static final String REP_URL = "http://ug3.technion.ac.il/rep/REPFILE.zip";
    private static final String REP_FILE_NAME = "REPY";
    private static final String REP_ENCODING = "Cp862";
    // no Cp862 in Android so use different encoding and then convert some chars
    // in
    // readLine method.
    // TODO more efficient solution
    private static final String REP_ENCODING_ANDROID = "ISO-8859-8";
    private static final String FACULTY_DELIMITER = "";
    private static final String COURSE_DELIMITER = "+------------------------------------------+";
    private static final String COURSE_DELIMITER_LONG = "+---------------------------------------------------------------+";
    private static final String COURSE_INNER_DELIMITER_LONG = "|                                             -----------       |";
    private static final String LECTURER_IN_CHARGE_TOKEN = "יארחא  הרומ";
    private static final String EXAM_A_TOKEN = "ןושאר דעומ";
    private static final String EXAM_B_TOKEN = "ינש דעומ";
    private static final String REGISTRATION_NUM_TOKEN = "|               ++++++                  .סמ|";
    private static final String SEMESTER_TOKEN = "רטסמס";
    private static final Matcher COURSE_NAME_HASH_FIX_MATCHER = Pattern.compile("\\D+\\s(:\\d+)").matcher("");
    private BufferedReader reader;
    private String line;
    private StringBuilder lineBuilder = new StringBuilder(COURSE_DELIMITER.length());
    private Map<Integer, Faculty> faculties;
    private Semester semester;
    private final boolean ugOnly = true;
    private final boolean isoDates = true;

    /**
     * Returns true if parser configured to parse only ug courses
     *
     * @return
     */
    public boolean isUgOnly() {
        return ugOnly;
    }

    /**
     * Returns true if parser configured to convert exams dates to ISO format
     *
     * @return
     */
    public boolean isIsoDates() {
        return isoDates;
    }

    private RepParser() {
    }

    public static RepParser newInstance() throws IOException {
        return newInstance(new File((File) null, REP_ZIP_FILE_NAME));
    }

    /**
     * @param file
     *          destination file
     * @throws IOException
     */
    public static RepParser newInstance(File file) throws IOException {
        try {
            downloadRep(file);
            return fromZipFile(file);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param filename
     *          REPY file
     * @return
     * @throws FileNotFoundException
     */
    public static RepParser fromFile(String filename) throws FileNotFoundException {
        RepParser parser = new RepParser();
        parser.parse(new FileInputStream(filename));
        return parser;
    }

    /**
     * @param file
     *          REPY file or zip file
     * @return
     * @throws FileNotFoundException
     */
    public static RepParser fromFile(File file) throws FileNotFoundException {
        if (file.getName().toLowerCase().endsWith(".zip"))
            return fromZipFile(file);
        else return fromFile(file.getPath());
    }

    public static RepParser fromZipFile(String filename) {
        return fromZipFile(new File(filename));
    }

    /**
     * @param file
     *          zip file
     * @return
     * @throws FileNotFoundException
     */
    public static RepParser fromZipFile(File file) {
        RepParser parser = new RepParser();
        ZipFile zipfile = null;
        try {
            zipfile = new ZipFile(file);
            parser.parse(zipfile.getInputStream(zipfile.getEntry(REP_FILE_NAME)));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (zipfile != null)
                try {
                    zipfile.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return parser;
    }

    /**
     * @param file
     *          the downloaded zip file destination
     * @throws IOException
     */
    public static void downloadRep(File file) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(REP_URL).openConnection();
        InputStream inputStream = conn.getInputStream();
        FileOutputStream outputStream = new FileOutputStream(file);
        byte[] buffer = new byte[8192];
        int n;
        while ((n = inputStream.read(buffer)) != -1)
            outputStream.write(buffer, 0, n);
        outputStream.close();
        conn.disconnect();
    }

    /**
     * @return parsed data as map (faculty number to Faculty object which contains
     *         faculty courses)
     */
    public Map<Integer, Faculty> getFaculties() {
        return faculties;
    }

    /**
     * Creates new map with all parsed courses
     *
     * @return parsed data as map (course number (Integer) to Course object
     */
    public Map<Integer, Course> getCourses() {
        Map<Integer, Course> result = new HashMap<>();
        for (Faculty f : faculties.values())
            for (Course c : f.getCourses())
                result.put(Integer.valueOf(c.number), c);
        return result;
    }

    public Semester getSemester() {
        return semester;
    }

    @Override public String toString() {
        return faculties.toString();
    }

    /**
     * Saves parsed data to JSON file
     *
     * @param filename
     * @throws IOException
     */
    public void toFile(String filename) throws IOException {
        FileWriter writer = new FileWriter(filename);
        writer.write(Json.toJSONString(faculties.values()));
        writer.close();
    }

    /**
     * Returns true if it is undergraduate course by specific rules <br>
     * <b>Warning:</b> this is only approximation and you need to check ug site to
     * be 100% sure.
     *
     * @param number
     *          course number (with leading zero e.g. 044145 not 44145)
     * @return true if it is undergraduate course
     */
    public static boolean isUgCourse(String number) {
        char c = number.charAt(2);
        // 8 or 9 is graduate course e.g. 048750, 049035
        // 320000-On Leave (hufshat limudim), 320003-Physical Education 1,
        // 320099-Students Exchange
        return c != '8' && c != '9'
                && !(number.startsWith("3200") && (number.equals("320000") || number.equals("320003") || number.equals("320099")));
    }

    // more restrictive
    private static boolean isUgCourse2(String number) {
        char c = number.charAt(2);
        // mechina
        if (c == '3')
            return false;
        // graduate
        if (c == '8' || c == '9')
            return false;
        // some courses in graduate, excellence program, mechina, exchange program
        return !number.startsWith("320");
    }

    /**
     * Parses and also closes the inputStream
     *
     * @param inputStream
     */
    private void parse(InputStream inputStream) {
        boolean lastUgFacultyReached = false;
        try {
            faculties = new LinkedHashMap<>();
            reader = new BufferedReader(new InputStreamReader(inputStream, REP_ENCODING_ANDROID));
            // skip first empty line
            readLines(2);
            while (!line.equals("")) {
                Faculty faculty = parseFaculty();
                if (!isUgOnly() || !lastUgFacultyReached)
                    faculties.put(faculty.getNumber(), faculty);
                if (faculty.number == 33)
                    lastUgFacultyReached = true;
                readLine();
            }
            parseSportCourses();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    private Faculty parseFaculty() {
        Faculty faculty = new Faculty();
        List<Course> courses = new ArrayList<>();
        readLine();
        faculty.setName(reverse(line.substring(1, line.indexOf("-")).trim()));
        // skip semester and footer and first course delimiter
        readLines(4);
        while (!line.equals(FACULTY_DELIMITER)) {
            Course course = parseCourse();
            if (!isUgOnly() || isUgCourse(course.number))
                courses.add(course);
            // skip course delimiter
            readLine();
        }
        faculty.setCourses(courses);
        if (courses.size() > 0)
            faculty.setNumber(Integer.parseInt(courses.get(0).getNumber().substring(0, 2)));
        return faculty;
    }

    private Course parseCourse() {
        Course course = new Course();
        setNameNumberPoints(course);
        while (!line.equals(REGISTRATION_NUM_TOKEN) && !line.equals(COURSE_DELIMITER)) {
            if (line.contains(LECTURER_IN_CHARGE_TOKEN))
                setLecturerInCharge(course);
            else if (line.contains(EXAM_A_TOKEN))
                course.setExamA((extractExamDate()));
            else if (line.contains(EXAM_B_TOKEN))
                course.setExamB((extractExamDate()));
            readLine();
        }
        // skip registration info
        while (!line.equals(COURSE_DELIMITER))
            readLine();
        return course;
    }

    /**
     * Adds sport courses to Humanities and Arts and also assigns semester to
     * instance variable
     */
    private void parseSportCourses() {
        List<Course> courses = faculties.get(32).getCourses();
        while (line.equals(""))
            readLine();
        readLine();
        semester = new Semester(reverse(line.substring(1, line.indexOf(SEMESTER_TOKEN)).trim()));
        // skip semester and footer and first course delimiter
        readLines(3);
        while (line != null) {
            courses.add(parseSportCourse());
            // skip course delimiter
            readLine();
        }
    }

    private Course parseSportCourse() {
        Course course = new Course();
        setNameNumberPoints(course);
        while (!line.equals(COURSE_INNER_DELIMITER_LONG)) {
            if (line.contains(LECTURER_IN_CHARGE_TOKEN))
                setLecturerInCharge(course);
            readLine();
        }
        // skip registration info
        while (!line.equals(COURSE_DELIMITER_LONG))
            readLine();
        return course;
    }

    private void setNameNumberPoints(Course course) {
        // name and number
        line = reverseBidi(extractInnerText(line));
        int index = line.indexOf(" ");
        course.setNumber(line.substring(0, index));
        course.setName(line.substring(index + 2));
        // fix rep (and ug) problem when course contains # instead of :
        if (course.getName().contains("#")) {
            course.setName(course.getName().replace("#", ":"));
            // fix cases (two that was found: 204202 and 207700) that reverseBidi
            // not reverse e.g. 2# instead of #2
            COURSE_NAME_HASH_FIX_MATCHER.reset(course.getName());
            if (COURSE_NAME_HASH_FIX_MATCHER.find()) {
                String s = COURSE_NAME_HASH_FIX_MATCHER.group(1);
                course.setName(course.getName().replace(s, reverse(s)));
            }
        }
        // for rare cases which have redundant spaces e.g. course number 54500
        if (course.getName().contains("  "))
            course.setName(course.getName().replaceAll("\\s+", " "));
        readLine();
        course.setPoints(Double.parseDouble(extractInnerText(line).substring(0, 3)));
        // skip footer
        readLine().readLine();
    }

    /**
     * Reads a line from reader and assign it to line instance variable
     *
     * @return this
     */
    private RepParser readLine() {
        try {
            line = reader.readLine();
            /** needed only when using REP_ENCODING_ANDROID **/
            if (line == null)
                return this;
            lineBuilder.delete(0, lineBuilder.length());
            for (char c : line.toCharArray()) {
                if (c < 128)
                    lineBuilder.append(c);
                else lineBuilder.append((char) (c + 1360));
            }
            line = lineBuilder.toString();
            /***********************************************/
        } catch (IOException e) {
            line = null;
        }
        return this;
    }

    /**
     * Reads number of lines from reader and assign the last line to line instance
     * variable
     *
     * @return
     */
    private RepParser readLines(int number) {
        for (int i = 0; i < number; i++)
            readLine();
        return this;
    }

    private void setLecturerInCharge(Course course) {
        line = extractInnerText(line);
        course.setLecturerInCharge(reverse(line.substring(0, line.indexOf(" "))));
    }

    /**
     * @return date in format e.g. 31/12/15. If ISO_DATES set to true returns e.g.
     *         2015-12-31.<br> Warning: iso date formatting assumes the rep file
     *         is from year 2000 and above
     */
    private String extractExamDate() {
        String date = line.substring(14, 22);
        if (!isIsoDates())
            return date;
        String[] s = date.split("/");
        return s.length < 3 ? null : String.format("20%s-%s-%s", s[2], s[1], s[0]);
    }

    /**
     * Removes | and spaces e.g. "|   text |" returns "text"
     *
     * @param text
     * @return
     */
    private static String extractInnerText(String text) {
        return text.substring(1, text.length() - 1).trim();
    }

    private static String reverse(String text) {
        return new StringBuilder(text).reverse().toString();
    }

    /**
     * This is very simple and not general visual to logical ordering converter
     *
     * @param text
     * @return
     */
    private static String reverseBidi(String text) {
        StringBuilder result = new StringBuilder();
        Bidi bidi = new Bidi(text, Bidi.DIRECTION_RIGHT_TO_LEFT);
        for (int i = bidi.getRunCount() - 1; i >= 0; i--) {
            int start = bidi.getRunStart(i);
            int end = bidi.getRunLimit(i);
            // even level is left-to-right so no need to reverse
            if ((bidi.getRunLevel(i) & 1) == 0)
                result.append(text, start, end);
            else for (; end-- > start;) {
                // fix brackets
                char c = text.charAt(end);
                if (c == '(')
                    c = ')';
                else if (c == ')')
                    c = '(';
                result.append(c);
            }
        }
        return result.toString();
    }

    /**
     * Converts hebrew text to number e.g. חורף תשע"ה to 20151
     */
    public static class Semester {
        public static final String WINTER = "חורף";
        private final String hebName;
        private int year;
        // winter is 1, spring is 2 (no rep file for summer)
        private final int semester;

        public int getYear() {
            return year;
        }

        public int getSemester() {
            return semester;
        }

        public String getHebName() {
            return hebName;
        }

        /**
         * @param hebName
         *          e.g. חורף תשע"ה
         */
        public Semester(String hebName) {
            this.hebName = hebName;
            String[] s = hebName.split(" ");
            semester = s[0].equals(WINTER) ? 1 : 2;
            // create char to number mapping
            Map<Character, Integer> charToNumber = new HashMap<>();
            String hebChars = " אבגדהוזחט יכלמנסעפצ קרשת";
            for (int i = 0; i < hebChars.length(); i++)
                charToNumber.put(hebChars.charAt(i), i % 10 * (int) Math.pow(10, i / 10));
            // calculate year
            for (char c : s[1].toCharArray()) {
                Integer i = charToNumber.get(c);
                year += (i == null ? 0 : i);
            }
            year += 1240;
        }

        public int toInt() {
            return year * 10 + semester;
        }

        @Override public String toString() {
            return hebName;
        }

        @Override public int hashCode() {
            return toInt();
        }

        @Override public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Semester other = (Semester) obj;
            return year == other.year && semester == other.semester;
        }
    }

    public static class Faculty {
        private int number;
        private String name;
        private List<Course> courses;

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Course> getCourses() {
            return courses;
        }

        public void setCourses(List<Course> courses) {
            this.courses = courses;
        }

        @Override public String toString() {
            return String.format("Faculty %s %s has %s courses]", number, name, courses.size());
        }

        @Override public int hashCode() {
            return number;
        }

        @Override public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Faculty other = (Faculty) obj;
            return number == other.number;
        }
    }

    public static class Course {
        // string to preserve leading zero e.g. 044145
        private String number;
        private String name;
        private double points;
        private String lecturerInCharge;
        // e.g. 23/02/15 or 2015-02-23 if isoDates set to true
        private String examA;
        private String examB;

        public String getNumber() {
            return number;
        }

        public Course setNumber(String number) {
            this.number = number;
            return this;
        }

        public String getName() {
            return name;
        }

        public Course setName(String name) {
            this.name = name;
            return this;
        }

        public double getPoints() {
            return points;
        }

        public Course setPoints(double points) {
            this.points = points;
            return this;
        }

        public String getLecturerInCharge() {
            return lecturerInCharge;
        }

        public Course setLecturerInCharge(String lecturerInCharge) {
            this.lecturerInCharge = lecturerInCharge;
            return this;
        }

        public String getExamA() {
            return examA;
        }

        public Course setExamA(String examA) {
            this.examA = examA;
            return this;
        }

        public String getExamB() {
            return examB;
        }

        public Course setExamB(String examB) {
            this.examB = examB;
            return this;
        }

        @Override public String toString() {
            return String.format("Course [number=%s, name=%s, points=%s, lecturerInCharge=%s, examA=%s, examB=%s]", number, name, points,
                    lecturerInCharge, examA, examB);
        }
    }

    /**
     * Simple JSON string writer for writing parsed data as JSON. For more options
     * use: http://mvnrepository.com/artifact/org.json/json (built-in in android)
     * <br>
     *
     * @see <a href="http://www.json.org/">json.org</a>
     */
    public static class Json {
        public static final String FACULTY_NAME = "facultyName";
        public static final String FACULTY_NUMBER = "facultyNumber";
        public static final String FACULTY_COURSES = "facultyCourses";
        public static final String COURSE_NAME = "courseName";
        public static final String COURSE_NUMBER = "courseNumber";
        public static final String COURSE_POINTS = "coursePoints";
        public static final String LECTURER_IN_CHARGE = "lecturerInCharge";
        public static final String EXAM_A = "examA";
        public static final String EXAM_B = "examB";
        private final StringBuilder sb = new StringBuilder();

        public static String toJSONString(Course course) {
            return new Json().startObject().put(COURSE_NAME, course.name).put(COURSE_NUMBER, course.number)
                    .put(COURSE_POINTS, course.points).put(LECTURER_IN_CHARGE, course.lecturerInCharge).put(EXAM_A, course.examA)
                    .put(EXAM_B, course.examB).endObject().toString();
        }

        public static String toJSONString(Faculty faculty) {
            Json json = new Json();
            json.startObject().put(FACULTY_NUMBER, faculty.number).put(FACULTY_NAME, faculty.name);
            // courses
            Json coursesJson = new Json().startArray();
            for (Course course : faculty.getCourses())
                coursesJson.put(Json.toJSONString(course));
            coursesJson.endArray();
            json.put(FACULTY_COURSES, coursesJson).endObject();
            return json.toString();
        }

        public static String toJSONString(Collection<Faculty> faculties) {
            Json json = new Json().startArray();
            for (Faculty faculty : faculties)
                json.put(Json.toJSONString(faculty));
            return json.endArray().toString();
        }

        @Override public String toString() {
            return sb.toString();
        }

        private Json put(String key, String value) {
            putWithQuotes(key);
            sb.append(":");
            putWithQuotes(value);
            sb.append(",");
            return this;
        }

        private Json put(String key, Number value) {
            putWithQuotes(key);
            sb.append(":").append(value).append(",");
            return this;
        }

        private Json put(String key, Json value) {
            putWithQuotes(key);
            sb.append(":").append(value.toString()).append(",");
            return this;
        }

        /**
         * Put array value
         *
         * @param value
         * @return
         */
        private Json put(String value) {
            sb.append(value).append(",");
            return this;
        }

        private Json startObject() {
            sb.append("{");
            return this;
        }

        private Json endObject() {
            // remove last comma
            if (sb.length() > 1)
                sb.setLength(sb.length() - 1);
            sb.append("}");
            return this;
        }

        private Json startArray() {
            sb.append("[");
            return this;
        }

        private Json endArray() {
            // remove last comma
            if (sb.length() > 1)
                sb.setLength(sb.length() - 1);
            sb.append("]");
            return this;
        }

        /**
         * For keys and string values
         */
        private void putWithQuotes(String value) {
            if (value == null)
                sb.append((char[]) null);
            else sb.append("\"").append(value.contains("\"") ? value.replace("\"", "\\\"") : value).append("\"");
        }
    }
}
