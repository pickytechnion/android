package il.ac.technion.picky;

import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

/**
 * Created by ohad on 8/23/15.
 */
public class TealSnackbar {

    public static void show(View view, String string) {
        make(view, string).show();
    }

    public static Snackbar make(View view, String string) {
        Snackbar snackbar = Snackbar.make(view, string, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimaryDark));
        return snackbar;
    }
}
