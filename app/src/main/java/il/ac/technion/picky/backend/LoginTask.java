package il.ac.technion.picky.backend;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ProgressBar;

import com.appspot.pickybackend.picky.model.Result;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

import java.io.IOException;
import java.security.MessageDigest;

import il.ac.technion.picky.R;
import il.ac.technion.picky.TealSnackbar;
import il.ac.technion.picky.util.GradeSheetParser;
import il.ac.technion.picky.util.GradeSheetParser.GradeSheetParserException;

import static il.ac.technion.picky.backend.ApiService.API;

/**
 * Created by ohad on 3/23/15.
 */
public class LoginTask extends AsyncTask<String, Void, Result> {

    private final Activity activity;
    private final ProgressBar progressBar;

    public LoginTask(Activity activity) {
        this.activity = activity;
        progressBar = (ProgressBar) activity.findViewById(R.id.signinProgressBar);
        progressBar.setVisibility(View.VISIBLE);
    }

    protected Result doInBackground(String... args) {
        Result result = new Result().setSuccess(false);
        try {
            GradeSheetParser parser = new GradeSheetParser(args[0], args[1]);
            String id = parser.getId();
            while (id.length() < 9)
                id = "0" + id;
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(id.getBytes());
            result = API.addUgGrades(new String(messageDigest.digest()), parser.getGrades()).execute();
            result.setMessage(activity.getString(R.string.login_after_share));
        } catch (GoogleJsonResponseException e) {
            if (ApiService.isOverQuotaException(e))
                result.setMessage(activity.getString(R.string.backend_over_quota));
            else
                result.setMessage(activity.getString(R.string.backend_error));
        } catch (IOException e) {
            result.setMessage(activity.getString(R.string.network_problem));
        } catch (GradeSheetParserException e) {
            if (e.getMessage().equals(GradeSheetParserException.WRONG_ID_OR_PASSWORD))
                result.setMessage(activity.getString(R.string.login_wrong_details));
            else if (e.getMessage().equals(GradeSheetParserException.NETWORK_PROBLEM))
                result.setMessage(activity.getString(R.string.network_problem));
            else if (e.getMessage().equals(GradeSheetParserException.NOT_ELIGIBLE)) {
                result.setMessage(activity.getString(R.string.login_not_eligible));
                result.setSuccess(true);
            }
        } catch (Exception e) {
            result.setMessage(activity.getString(R.string.exception_general));
        }
        return result;
    }

    @Override
    protected void onPostExecute(Result result) {
        progressBar.setVisibility(View.INVISIBLE);
        Snackbar snackbar = TealSnackbar.make(activity.findViewById(R.id.login_activity_screen), result.getMessage());
        if (result.getSuccess()) {
            // set result in LoginActivity that it will return to CourseListActivity
            activity.setResult(Activity.RESULT_OK);
            snackbar.setCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    activity.finish();
                }
            });
        }
        snackbar.show();
    }

}
