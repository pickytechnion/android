package il.ac.technion.picky.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses UG grade sheet to JSON. Only parses course number and grade i.e.
 * numeric values. Ignores credits ("zikuyim") table.
 *
 */
public class GradeSheetParser {
    @SuppressWarnings("serial") public static class GradeSheetParserException extends Exception {
        public static final String WRONG_ID_OR_PASSWORD = "WRONG_ID_OR_PASSWORD";
        public static final String NOT_ELIGIBLE = "NOT_ELIGIBLE";
        public static final String NETWORK_PROBLEM = "NETWORK_PROBLEM";

        public GradeSheetParserException(String message) {
            super(message);
        }
    }

    private static final String MAIN_URL = "http://techmvs.technion.ac.il/cics/wmn/wmngrad?ORD=1";
    private static final String URL_SUFFIX = "&s=1";
    private static final int REDIRECTIONS_LIMIT = 50;
    // setting User-Agent is optional
    private static final String USER_AGENT = "Mozilla/5.0";
    private static final String POST_DATA_FORMAT = "function=signon&userid=%s&password=%s";
    private static final String ID_KEY = "id";
    private static final String SEMESTERS_KEY = "semesters";
    // example: for input "(ה"עשת)2014/15 ףרוח" this pattern captures 20, 15, ףרוח
    private static final Matcher MATCHER_SEMESTER = Pattern.compile("\\D+(\\d{2})\\d{2}/(\\d{2})(\\D+)").matcher("");
    private static final Matcher MATCHER_GRADE = Pattern.compile("\\d+").matcher("");
    private static final char NBSP = (char) 160;
    private static final String CREDITS = "םייוכיז";
    private static final String NOT_ELIGIBLE = "הז תוריש תלבקל יאכז ךניא";
    private final String html;
    private JSONObject gradeSheet;
    private int gradesCount;

    public String getHtml() {
        return html;
    }

    public JSONObject getGradeSheet() {
        return gradeSheet;
    }

    public String getId() throws JSONException {
        return gradeSheet.getString(ID_KEY);
    }

    public String getGrades() throws JSONException {
        return gradeSheet.getJSONArray(SEMESTERS_KEY).toString();
    }

    public int getGradesCount() {
        return gradesCount;
    }

    public GradeSheetParser(String html) throws JSONException {
        this.html = html;
        parse();
    }

    public GradeSheetParser(String id, String password) throws IOException, GradeSheetParserException, JSONException {
        this(fetchHtml(id, password));
    }

    public GradeSheetParser(File file) throws IOException, JSONException {
        this(streamToString(new FileInputStream(file), "UTF8"));
    }

    private void parse() throws JSONException {
        gradeSheet = new JSONObject();
        JSONArray semesters = new JSONArray();
        Elements tables = Jsoup.parse(html).select("table");
        // skip table 0 (header).
        // table 1 is details.
        parseDetails(tables.get(1));
        // skip table 2 (general grades).
        // check if Credits (zikuyim) exist and skip it
        // (TODO: maybe no need to check because it is always exists)
        int i = tables.get(3).select("tr:eq(0) > td:eq(0)").text().equals(CREDITS) ? 4 : 3;
        for (; i < tables.size(); i++)
            parseSemester(tables.get(i), semesters);
        gradeSheet.put(SEMESTERS_KEY, semesters);
    }

    private void parseDetails(Element table) throws JSONException {
        gradeSheet.put(ID_KEY, table.select("tr:eq(1) > td:eq(0)").text());
    }

    private void parseSemester(Element table, JSONArray semesters) {
        JSONArray semester = new JSONArray();
        Elements rows = table.select("tr");
        putSemesterName(semester, rows.get(0).select("tr:eq(0) > td:eq(0)").text());
        JSONArray courses = new JSONArray();
        // last row contains total semester info so skip it
        for (int i = 2; i < rows.size() - 1; i++) {
            if (!MATCHER_GRADE.reset(rows.get(i).select("td:eq(0)").text()).find())
                continue;
            gradesCount++;
            JSONArray course = new JSONArray();
            String nameAndNumber = rows.get(i).select("td:eq(2)").text();
            // course number
            course.put(Integer.parseInt(nameAndNumber.substring(nameAndNumber.lastIndexOf(NBSP) + 1)));
            // grade
            course.put(Integer.parseInt(MATCHER_GRADE.group()));
            courses.put(course);
        }
        semester.put(courses);
        // don't add semester with no grades e.g. semester before exams
        if (courses.length() == 0)
            return;
        semesters.put(semester);
    }

    private static void putSemesterName(JSONArray semester, String name) {
        MATCHER_SEMESTER.reset(name).find();
        // year
        semester.put(Integer.parseInt((MATCHER_SEMESTER.group(1) + MATCHER_SEMESTER.group(2))));
        // semester
        semester.put(semesterToInt(MATCHER_SEMESTER.group(3)));
    }

    /**
     * Finds by unique character in semester name
     *
     * @param semester
     *          hebrew name (can be reversed with NBSP)
     * @return 1/2/3 for winter/spring/summer
     */
    private static int semesterToInt(String semester) {
        if (semester.contains("ח"))
            return 1;
        if (semester.contains("א"))
            return 2;
        if (semester.contains("ק"))
            return 3;
        return -1; // error
    }

    /***********************
     * fetch html methods
     **********************/
    /**
     * There could be multiple redirections by the server (302 code) so we wait
     * until response is 200
     *
     * @return
     * @throws IOException
     */
    private static String getRedirectedUrl() throws IOException {
        int redirectsNum = 0;
        String url = MAIN_URL;
        HttpURLConnection conn = null;
        do {
            if (conn != null)
                conn.disconnect();
            conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestProperty("User-Agent", USER_AGENT);
            conn.setInstanceFollowRedirects(false);
            url = conn.getHeaderField("Location");
        } while (conn.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP && ++redirectsNum < REDIRECTIONS_LIMIT);
        url = conn.getURL().toString();
        conn.disconnect();
        return url;
    }

    private static String fetchHtml(String id, String password) throws IOException, GradeSheetParserException {
        String result = getRedirectedUrl();
        if (result == null)
            throw new GradeSheetParserException(GradeSheetParserException.NETWORK_PROBLEM);
        // new connection from redirected url
        HttpURLConnection conn = (HttpURLConnection) new URL(result + URL_SUFFIX).openConnection();
        conn.setRequestProperty("User-Agent", USER_AGENT);
        // we want to handle the post redirection manually
        conn.setInstanceFollowRedirects(false);
        // Referer is necessary but removing :80 is optional
        conn.setRequestProperty("Referer", result.replace(":80", ""));
        // POST request
        conn.setDoOutput(true);
        String postData = String.format(POST_DATA_FORMAT, id, password);
        conn.setFixedLengthStreamingMode(postData.length());
        // write post data to the request
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
        writer.write(postData);
        writer.close();
        // send request and get cookie e.g. TechAnamUser=12345678
        String cookie = conn.getHeaderField("Set-Cookie");
        conn.disconnect();
        // dirty way to check wrong ID/password
        if (cookie == null)
            throw new GradeSheetParserException(GradeSheetParserException.WRONG_ID_OR_PASSWORD);
        // Cookie is necessary but this setting is optional
        cookie = cookie.substring(0, cookie.indexOf(";")).replace(" ", "");
        conn = (HttpURLConnection) new URL(result).openConnection();
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Cookie", cookie);
        result = streamToString(conn.getInputStream(), "Cp1255");
        conn.disconnect();
        if (result.contains(NOT_ELIGIBLE))
            throw new GradeSheetParserException(GradeSheetParserException.NOT_ELIGIBLE);
        return result;
    }

    private static String streamToString(InputStream stream, String charset) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, charset));
        StringWriter writer = new StringWriter();
        char[] buffer = new char[8192];
        int n;
        while ((n = reader.read(buffer)) != -1)
            writer.write(buffer, 0, n);
        return writer.toString();
    }
}
