package il.ac.technion.picky.data;


import android.app.AlertDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import il.ac.technion.picky.CourseListActivity;
import il.ac.technion.picky.R;
import il.ac.technion.picky.TealSnackbar;
import il.ac.technion.picky.util.RepParser;


public class RepUpdater extends AsyncTask<Void, Void, Void> {

    private static final String TAG = RepUpdater.class.getSimpleName();
    private final CourseListActivity activity;
    private Exception exception;
    // courses numbers that changed/added/removed in courses table or courses_by_semester TODO detailed courses changes
    private final Set<String> coursesUpdated;
    private boolean newSemester;

    public RepUpdater(CourseListActivity activity) {
        this.activity = activity;
        coursesUpdated = new TreeSet<>();
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.d(TAG, "start rep update");
        DataOpenHelper db = null;
        try {
            File repFile = new File(activity.getCacheDir(), RepParser.REP_ZIP_FILE_NAME);
            long oldRepFileLength = !repFile.exists() ? 0 : repFile.length();
            RepParser.downloadRep(repFile);
            if (repFile.length() == oldRepFileLength)
                return null;
            RepParser parser = RepParser.fromZipFile(repFile);
            RepParser.Semester semester = parser.getSemester();
            db = new DataOpenHelper(activity);
            newSemester = semester.toInt() > db.getMaxSemester();
            db.deleteRepData();
            db.insertRepData(parser.getFaculties().values());
            // courses table changes
            Cursor cursor = db.getRepCoursesChanges();
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext())
                    addUpdatedCourse(cursor.getInt(0));
                db.updateCoursesByRep();
            }
            /**** courses by semester table changes ***/
            // delete removed courses
            if (!newSemester) {
                cursor = db.getRepCoursesBySemestersRemoved(semester.getYear(), semester.getSemester());
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext())
                        addUpdatedCourse(cursor.getInt(0));
                    db.deleteRemovedCoursesBySemestersByRep(semester.getYear(), semester.getSemester());
                }
            }
            // added and changed courses
            cursor = db.getRepCoursesBySemestersChanges(semester.getYear(), semester.getSemester());
            if (cursor.getCount() > 0) {
                // in new semester all the courses are changed so we don't include them
                if (!newSemester) {
                    while (cursor.moveToNext())
                        addUpdatedCourse(cursor.getInt(0));
                }
                db.updateCoursesBySemestersByRep(semester.getYear(), semester.getSemester());
            }
        } catch (IOException e) {
            exception = e;
            e.printStackTrace();
        } finally {
            if (db != null)
                db.close();
        }
        Log.d(TAG, "end rep update");
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        String message;
        if (exception != null)
            message = activity.getString(R.string.exception_general);
        else {
            if (newSemester) {
                activity.notifySemestersChanged();
                activity.notifyCoursesChanged();
                message = activity.getString(R.string.rep_new_semester);
            }
            else {
                if (coursesUpdated.size() == 0)
                    message = activity.getString(R.string.rep_no_updates);
                else {
                    activity.notifyCoursesChanged();
                    message = activity.getString(R.string.rep_updates);
                }
            }
        }
        Snackbar snackbar = TealSnackbar.make(activity.findViewById(R.id.coordinator), message);
        if (coursesUpdated.size() > 0)
            snackbar.setAction(activity.getString(R.string.rep_see_updates), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(activity)
                            .setTitle(String.valueOf(coursesUpdated.size()) + " " + activity.getString(R.string.rep_updates_title))
                            .setItems(coursesUpdated.toArray(new String[coursesUpdated.size()]), null)
                            .create().show();
                }
            });
        SwipeRefreshLayout mSwipeRefreshLayout = (SwipeRefreshLayout) activity.findViewById(R.id.swipe_to_refresh);
        mSwipeRefreshLayout.setRefreshing(false);
        snackbar.show();
    }

    private void addUpdatedCourse(int id) {
        coursesUpdated.add(String.format("%06d", id));
    }
}
