package il.ac.technion.picky.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import il.ac.technion.picky.R;
import il.ac.technion.picky.util.Semester;

public class DataProvider extends ContentProvider {

    // unique symbolic name of the entire provider
    private static final String AUTHORITY = "il.ac.technion.picky.provider";
    private static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    // name that points to table/view
    private static final String FACULTIES_PATH = DataContract.Faculty.TABLE_NAME;
    private static final String COURSES_PATH = DataContract.Course.TABLE_NAME;
    private static final String COURSES_WITH_FACULTY_INITIAL_PATH = DataContract.CourseWithFacultyInitial.TABLE_NAME;
    private static final String COURSES_BY_SEMESTER_PATH = DataContract.CourseBySemester.TABLE_NAME;
    private static final String COURSES_WITH_EXAMS_PATH = DataContract.CourseWithExams.TABLE_NAME;
    private static final String WITH_ALL_ROW_PATH = "with_all_row";

    private static final int FACULTIES = 1;
    private static final int COURSES = 2;
    private static final int COURSE = 3;
    private static final int POINTS = 4;
    private static final int FACULTIES_NAMES = 5;
    private static final int SEMESTERS = 6;
    private static final int COURSES_WITH_FACULTY_INITIAL = 7;
    private static final int COURSES_WITH_EXAMS = 8;
    private static final int COURSES_WITH_EXAMS_GROUP_BY_COURSE = 9;
    private static final int FACULTIES_WITH_ALL_ROW = 10;
    private static final int SEMESTERS_WITH_ALL_ROW = 11;

    // map URI patterns to integer values to decode incoming URIs.
    // UriMatcher.NO_MATCH is integer value to return if a given URI matches none of the patterns
    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(AUTHORITY, FACULTIES_PATH, FACULTIES);
        URI_MATCHER.addURI(AUTHORITY, FACULTIES_PATH + "/names", FACULTIES_NAMES);
        URI_MATCHER.addURI(AUTHORITY, COURSES_PATH, COURSES);
        URI_MATCHER.addURI(AUTHORITY, COURSES_PATH + "/#", COURSE);
        URI_MATCHER.addURI(AUTHORITY, COURSES_PATH + "/points", POINTS);
        URI_MATCHER.addURI(AUTHORITY, COURSES_BY_SEMESTER_PATH + "/semesters", SEMESTERS);
        URI_MATCHER.addURI(AUTHORITY, COURSES_WITH_FACULTY_INITIAL_PATH, COURSES_WITH_FACULTY_INITIAL);
        URI_MATCHER.addURI(AUTHORITY, COURSES_WITH_EXAMS_PATH, COURSES_WITH_EXAMS);
        URI_MATCHER.addURI(AUTHORITY, COURSES_WITH_EXAMS_PATH + "/group_by_course", COURSES_WITH_EXAMS_GROUP_BY_COURSE);
        URI_MATCHER.addURI(AUTHORITY, FACULTIES_PATH + "/" + WITH_ALL_ROW_PATH, FACULTIES_WITH_ALL_ROW);
        URI_MATCHER.addURI(AUTHORITY, COURSES_BY_SEMESTER_PATH + "/semesters/" + WITH_ALL_ROW_PATH, SEMESTERS_WITH_ALL_ROW);
    }

    /**
     * content URI is a URI that identifies data in a provider e.g.
     * content://il.ac.technion.picky.provider/courses_by_semesters/semesters
     */
    public static final Uri COURSES_URI = CONTENT_URI.buildUpon().appendPath(COURSES_PATH).build();
    public static final Uri FACULTIES_URI = CONTENT_URI.buildUpon().appendPath(FACULTIES_PATH).build();
    public static final Uri POINTS_URI = CONTENT_URI.buildUpon().appendPath(COURSES_PATH).appendPath("points").build();
    public static final Uri FACULTIES_NAMES_URI = CONTENT_URI.buildUpon().appendPath(FACULTIES_PATH).appendPath("names").build();
    public static final Uri SEMESTERS_URI = CONTENT_URI.buildUpon().appendPath(COURSES_BY_SEMESTER_PATH).appendPath("semesters").build();
    public static final Uri COURSES_WITH_FACULTY_INITIAL_URI = CONTENT_URI.buildUpon().appendPath(COURSES_WITH_FACULTY_INITIAL_PATH).build();
    public static final Uri COURSES_WITH_EXAMS_URI = CONTENT_URI.buildUpon().appendPath(COURSES_WITH_EXAMS_PATH).build();
    public static final Uri COURSES_WITH_EXAMS_GROUP_BY_COURSE_URI = CONTENT_URI.buildUpon().appendPath(COURSES_WITH_EXAMS_PATH).appendPath("group_by_course").build();
    public static final Uri FACULTIES_WITH_ALL_ROW_URI = FACULTIES_URI.buildUpon().appendPath(WITH_ALL_ROW_PATH).build();
    public static final Uri SEMESTERS_WITH_ALL_ROW_URI = SEMESTERS_URI.buildUpon().appendPath(WITH_ALL_ROW_PATH).build();


    private DataOpenHelper dataOpenHelper;

    @Override
    public boolean onCreate() {
        dataOpenHelper = new DataOpenHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dataOpenHelper.getReadableDatabase();
        Cursor cursor;
        int match = URI_MATCHER.match(uri);
        switch (match) {
            case COURSES_WITH_FACULTY_INITIAL:
                cursor = db.query(DataContract.CourseWithFacultyInitial.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case COURSE:
                cursor = db.query(DataContract.Course.TABLE_NAME, projection, DataContract.Course._ID + "=" + uri.getLastPathSegment(), selectionArgs, null, null, sortOrder);
                break;
            case FACULTIES:
            case FACULTIES_WITH_ALL_ROW:
                cursor = db.query(DataContract.Faculty.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                if (match == FACULTIES_WITH_ALL_ROW)
                    cursor = addAllRow(cursor);
                break;
            case FACULTIES_NAMES:
                cursor = db.query(DataContract.Faculty.TABLE_NAME, new String[]{DataContract.Faculty._ID, DataContract.Faculty.NAME}, selection, selectionArgs, null, null, sortOrder);
                break;
            case POINTS:
                cursor = db.query(DataContract.Course.TABLE_NAME, new String[]{DataContract.Course.POINTS}, null, null, DataContract.Course.POINTS, null, sortOrder);
                break;
            case SEMESTERS:
            case SEMESTERS_WITH_ALL_ROW:
                String semesterCol = DataContract.CourseBySemester.YEAR + "||" + DataContract.CourseBySemester.SEMESTER;
                cursor = db.query(DataContract.CourseBySemester.TABLE_NAME, new String[]{semesterCol + " AS _id"}, null, null, semesterCol, null, semesterCol + " DESC");
                if (match == SEMESTERS_WITH_ALL_ROW) {
                    MatrixCursor matrixCursor = new MatrixCursor(new String[]{DataContract.Faculty._ID, DataContract.Faculty.NAME}, 1);
                    while (cursor.moveToNext()) {
                        int semester = cursor.getInt(0);
                        matrixCursor.addRow(new Object[]{semester, new Semester(semester).toHebString()});
                    }
                    cursor = addAllRow(matrixCursor);
                }
                break;
            case COURSES_WITH_EXAMS:
                cursor = db.query(DataContract.CourseWithExams.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case COURSES_WITH_EXAMS_GROUP_BY_COURSE:
                cursor = db.query(DataContract.CourseWithExams.TABLE_NAME, projection, selection, selectionArgs, DataContract.Course._ID, null, sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Notify the Context's ContentResolver of the change
        getContext().getContentResolver().notifyChange(uri, null);
        return cursor;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    /**
     * Adds "ALL" option to cursor
     *
     * @param cursor with first column _ID
     * @return same input cursor with first row (-1, "all",...)
     */
    private Cursor addAllRow(Cursor cursor) {
        MatrixCursor matrixCursor = new MatrixCursor(cursor.getColumnNames(), cursor.getCount()+1);
        matrixCursor.newRow().add(-1).add(getContext().getString(R.string.all));
        return new MergeCursor(new Cursor[]{matrixCursor, cursor});
    }
}
