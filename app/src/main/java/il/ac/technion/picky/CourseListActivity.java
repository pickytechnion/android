package il.ac.technion.picky;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.jsoup.helper.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import il.ac.technion.picky.data.DataContract;
import il.ac.technion.picky.data.DataOpenHelper;
import il.ac.technion.picky.data.Filter;
import il.ac.technion.picky.data.RepUpdater;


/**
 * An activity representing a list of Courses. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link CourseDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link CourseListFragment} and the item details
 * (if present) is a {@link CourseDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link CourseListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class CourseListActivity extends AppCompatActivity
        implements CourseListFragment.Callbacks, SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private static final String DEFAULT_SORT = DataContract.Course.NAME;
    private static final String VIEWS_COUNT_KEY = "viewsCount";
    private static final int LOGIN_REQUEST = 0;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean twoPane;
    private boolean itemSelected = false;

    private CourseListFragment listFragment;
    private FilterFragment filterFragment;
    // for locking courses views from unregistered users
    private int viewsCount;
    private String searchViewFilter;
    private Filter filter;
    private String sort;
    private SearchView searchView;
    // Anchor view for sort PopupMenu when sort action button not visible
    private View sortAnchorView;
    private DrawerLayout drawerLayout;
    private View filterDrawer;
    private boolean examsMode;
    private ListPopupWindow sortPopup;
    private List<Map<String, Object>> sortAdapterData;
    private String[] sortAdapterColumns;
    private List<String> sortColumnsNames;
    private String[] sortTitles;
    private int sortExamsIndex;

    public boolean isTwoPane() {
        return twoPane;
    }

    Filter getFilter() {
        return filter;
    }

    void clearFilterFragmentFilter() {
        filter.clear(Filter.FilterKey.SEARCH_VIEW_NAME, Filter.FilterKey.SEARCH_VIEW_NUMBER);
    }

    String getSort() {
        return sort;
    }

    private void setSort(String sort) {
        setSort(sort, false);
    }

    private void setSort(String sort, boolean desc) {
        // remove old image
        int i;
        if (this.sort!=null && (i = sortColumnsNames.indexOf(this.sort.replace(" DESC","")))>=0 && i < sortAdapterData.size())
            sortAdapterData.get(i).put(sortAdapterColumns[1], null);
        // add new image
        sortAdapterData.get(sortColumnsNames.indexOf(sort.replace(" DESC",""))).put(sortAdapterColumns[1], desc || sort.contains(" DESC") ? R.drawable.ic_arrow_downward_black_24dp : R.drawable.ic_arrow_upward_black_24dp);

        this.sort = sort;
        if (desc)
            this.sort += " DESC";
    }

    void setExamMode(boolean examMode) {
        this.examsMode = examMode;
        setAdapters();
        setSortData();
    }

    private void setSortData() {
        if (examsMode) {
            // add exams sort items (if not already exist)
            if (sortAdapterData.size()==sortTitles.length)
                return;
            for (int i=sortExamsIndex; i < sortTitles.length; i++){
                HashMap<String, Object> row = new HashMap<>();
                row.put(sortAdapterColumns[0], sortTitles[i]);
                row.put(sortAdapterColumns[1], null);
                sortAdapterData.add(row);
            }
        } else {
            // remove exams sort items
            for (int i=sortAdapterData.size()-1; i >= sortExamsIndex; i--)
                sortAdapterData.remove(i);
            if (sort.contains(DataContract.CourseWithExams.EXAM_A_ISO) || sort.contains(DataContract.CourseWithExams.EXAM_B_ISO))
                setSort(DEFAULT_SORT);
        }
    }

    void setAdapters() {
        if (examsMode)
            listFragment.setExamsAdapter();
        else
            listFragment.setCoursesAdapter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_list);
        // force rtl
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        if (findViewById(R.id.course_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true;
//
//            // start empty fragment. we do this especcially for showing detail fragment icons
//            CourseDetailFragment fragment = new CourseDetailFragment();
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.course_detail_container, fragment)
//                    .commit();
        }

        sortPopup = new ListPopupWindow(this);
        sortPopup.setModal(true);
        sortAdapterData = new ArrayList<>();
        sortAdapterColumns = new String[]{"text", "image"};
        sortColumnsNames = new ArrayList<>(Arrays.asList(DataContract.Course.NAME, DataContract.Course.NUMBER,  DataContract.Course.POINTS, DataContract.CourseWithExams.EXAM_A_ISO, DataContract.CourseWithExams.EXAM_B_ISO));
        sortTitles = getResources().getStringArray(R.array.sort_with_exams);
        sortExamsIndex = 3;
        for (int i=0; i < sortExamsIndex; i++){
            HashMap<String, Object> row = new HashMap<>();
            row.put(sortAdapterColumns[0], sortTitles[i]);
            row.put(sortAdapterColumns[1], null);
            sortAdapterData.add(row);
        }
        sortPopup.setAdapter(new SimpleAdapter(this,
                sortAdapterData,
                R.layout.sort_item,
                sortAdapterColumns,
                new int[]{android.R.id.text1, R.id.sort_arrow_image}));
        // measure content width because ListPopupWindow.WRAP_CONTENT is not working as expected
        View v = getLayoutInflater().inflate(R.layout.sort_item, new FrameLayout(this) , false);
        int width = 0;
        for (String title: sortTitles) {
            ((TextView) v.findViewById(android.R.id.text1)).setText(title);
            v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            if (width < v.getMeasuredWidth())
                width = v.getMeasuredWidth();
        }
        ImageView imageView = ((ImageView) v.findViewById(R.id.sort_arrow_image));
        imageView.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
        imageView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        sortPopup.setContentWidth(width + imageView.getMeasuredWidth());
        sortPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String newSort = sortColumnsNames.get(i);
                // if new sort is the same reverse the order (startsWith because it can contain DESC)
                setSort(newSort, sort.startsWith(newSort) && !sort.contains(" DESC"));
                refreshList("sort");
                sortPopup.dismiss();
            }
        });

        listFragment = (CourseListFragment) getSupportFragmentManager().findFragmentById(R.id.course_list);
        filterFragment = (FilterFragment) getSupportFragmentManager().findFragmentById(R.id.filter_drawer);
        if (savedInstanceState == null) {
            filter = new Filter();
            sort = DEFAULT_SORT;
            examsMode = false;
        } else {
            filter = (Filter) savedInstanceState.getSerializable("filter");
            sort = savedInstanceState.getString("sort");
            examsMode = savedInstanceState.getBoolean("examsMode");
            searchViewFilter = savedInstanceState.getString("searchViewFilter");
        }
        setSortData();
        setSort(sort);

        viewsCount = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(VIEWS_COUNT_KEY, 0);
        ((PickyApplication) getApplication()).resetShowcaseTimes();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        filterDrawer = findViewById(R.id.filter_drawer);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.actionbar_icon);
        getSupportActionBar().setTitle(R.string.choose_course);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("sort", sort);
        outState.putSerializable("filter", filter);
        outState.putBoolean("examsMode", examsMode);
        outState.putString("searchViewFilter", searchViewFilter);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        ((PickyApplication) getApplication()).resetShowcaseTimes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_course_list, menu);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        sortAnchorView = menu.getItem(0).getActionView();

        // restore searchView on screen rotation
        if (searchViewFilter!=null) {
            String s = new String(searchViewFilter);
            menu.findItem(R.id.action_search).expandActionView();
            searchView.setQuery(s, false);
            searchViewFilter = s;
            refreshList("searchView");
        }
        // TODO check in twoPane
        //if (twoPane)
        //    getMenuInflater().inflate(R.menu.menu_course_details, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==LOGIN_REQUEST && resultCode==RESULT_OK){
            viewsCount = -1;
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putInt(VIEWS_COUNT_KEY, viewsCount).apply();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                startActivityForResult(new Intent(CourseListActivity.this, LoginActivity.class), LOGIN_REQUEST);
                return true;
            case R.id.action_filter:
                if (drawerLayout.isDrawerOpen(Gravity.LEFT))
                    drawerLayout.closeDrawer(filterDrawer);
                else
                    drawerLayout.openDrawer(filterDrawer);
                return true;
            case R.id.action_sort:
                View v = findViewById(R.id.action_sort);
                sortPopup.setAnchorView(v != null ? v : sortAnchorView);
                sortPopup.show();
                return true;
            case R.id.action_about:
                new AboutDialog().show(CourseListActivity.this.getSupportFragmentManager(), null);
                return true;
            case R.id.action_rep_update:
                SwipeRefreshLayout mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_to_refresh);
                mSwipeRefreshLayout.setRefreshing(true);
                new RepUpdater(CourseListActivity.this).execute();
                return true;
            // TODO check in twoPane
            case R.id.action_guide:
                if (!itemSelected)
                    TealSnackbar.show(this.findViewById(R.id.course_detail_container), getResources().getString(R.string.showcase_choose_course));
                return true;
            default:
                return false;
        }
    }

    /**
     * Callback method from {@link CourseListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(long id, String number, String name) {

        itemSelected = true;
        if (viewsCount == 6) {
            startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST);
            return;
        }

        Bundle arguments = new Bundle();
        arguments.putLong(CourseDetailFragment.ARG_ID, id);
        arguments.putString(CourseDetailFragment.ARG_NUMBER, number);

        if (twoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.

            CourseDetailFragment fragment = new CourseDetailFragment();
            fragment.setArguments(arguments);
            getFragmentManager().beginTransaction()
                    .replace(R.id.course_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, CourseDetailActivity.class);
            arguments.putString(CourseDetailFragment.ARG_NAME, name);
            detailIntent.putExtras(arguments);
            startActivity(detailIntent);
        }

        // user is registered
        if (viewsCount < 0) return;

        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putInt(VIEWS_COUNT_KEY, ++viewsCount).apply();
        if (viewsCount == 3 || viewsCount == 5)
            startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST);
    }

    public void refreshList(String source) {
        Log.d("refreshList", source);
        listFragment.restartLoader();
    }


    /******************
     * SearchView methods
     ******************/
    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(searchView.getQuery()))
            searchView.setQuery(null, true);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        String newFilter = TextUtils.isEmpty(s) ? null : s;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (searchViewFilter == null && newFilter == null)
            return true;
        if (searchViewFilter != null && searchViewFilter.equals(newFilter))
            return true;
        searchViewFilter = newFilter;
        if (searchViewFilter == null) {
            filter.remove(Filter.FilterKey.SEARCH_VIEW_NAME);
            filter.remove(Filter.FilterKey.SEARCH_VIEW_NUMBER);
        } else {
            if (StringUtil.isNumeric(searchViewFilter))
                filter.add(Filter.FilterKey.SEARCH_VIEW_NUMBER, searchViewFilter + "%");
            else
                filter.add(Filter.FilterKey.SEARCH_VIEW_NAME, "%" + searchViewFilter + "%");
        }
        refreshList("SEARCH_VIEW");
        return true;
    }

    /********************************************************/

    /**
     * Updates the courses list after rep update
     */
    public void notifyCoursesChanged() {
        refreshList("notifyCoursesChanged");
    }

    /**
     * Updates the semesters spinner after rep update
     */
    public void notifySemestersChanged() {
        filterFragment.refreshSemestersSpinner();
    }

    public static class AboutDialog extends DialogFragment {
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            View view = View.inflate(getActivity(), R.layout.about_dialog, null);
            String versionName = "";
            String versionCode = "";
            try {
                PackageInfo pi = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                versionName = pi.versionName;
                versionCode = String.valueOf(pi.versionCode);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            ((TextView) view.findViewById(R.id.version_name)).setText(versionName);
            ((TextView) view.findViewById(R.id.version_code)).setText(versionCode);
            ((TextView) view.findViewById(R.id.db_version)).setText(DataOpenHelper.getDBVersion());
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(view);
            return builder.create();
        }
    }
}
