package il.ac.technion.picky;

// this is for global variables. also added in manifest - android:name=".PickyApplication"

import android.app.Application;
import android.content.Context;

public class PickyApplication extends Application {

    private static final int SHOWCASE_LIMIT = 2;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getContext(){
        return context;
    }

    private int showcaseTimes;

    public void resetShowcaseTimes(){
        showcaseTimes = 0;
    }

    public boolean checkAndUpdateShowcaseTimes() {
        return showcaseTimes++<SHOWCASE_LIMIT;
    }



}
