package il.ac.technion.picky.backend;

import com.appspot.pickybackend.picky.model.PubReport;

public class PubReportWrapper {

    private final PubReport pubReport;
    private final long courseId;
    private final int year;
    private final int semester;

    public PubReportWrapper(Long courseId, int year, int semester, double grade, String moedName, String source, String comments){

        this.courseId=courseId;
        this.year=year;
        this.semester=semester;

        pubReport = new PubReport();
        pubReport.setFinalAvg(grade);
        pubReport.setMoed(moedName);
        pubReport.setSourceString(source);
        pubReport.setComments(comments);
        pubReport.setSource("User");
    }

    public PubReport getPubReport() {
        return pubReport;
    }

    public long getCourseId() {
        return courseId;
    }

    public int getYear() {
        return year;
    }

    public int getSemester() {
        return semester;
    }
}
