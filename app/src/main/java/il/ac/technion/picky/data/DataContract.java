package il.ac.technion.picky.data;

import android.provider.BaseColumns;

public class DataContract {

    public interface Faculty extends BaseColumns {

        String TABLE_NAME = "faculties";
        String NAME = "name";
        String INITIAL = "initial";
    }

    public interface Course extends BaseColumns {

        String TABLE_NAME = "courses";
        String NUMBER = "number";
        String NAME = "name";
        String POINTS = "points";
        String FACULTY = "faculty";
    }

    public interface CourseBySemester extends BaseColumns {

        String TABLE_NAME = "courses_by_semesters";
        String YEAR = "year";
        String SEMESTER = "semester";
        String HAS_EXAM = "hasExam";
        String EXAM_A_ISO = "examAISO";
        String EXAM_B_ISO = "examBISO";
    }

    // view
    public static abstract class CourseWithFacultyInitial implements Course {
        // hides Course TABLE_NAME constant
        public static final String TABLE_NAME = "courses_with_faculties";
        public static final String FACULTY_INITIAL = "facultyInitial";
    }

    // view
    public static abstract class CourseWithExams extends CourseWithFacultyInitial implements CourseBySemester {

        public static final String TABLE_NAME = "courses_with_exams";
        public static final String EXAM_A = "examA";
        public static final String EXAM_B = "examB";
    }

}
