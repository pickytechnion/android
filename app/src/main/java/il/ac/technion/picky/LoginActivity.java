package il.ac.technion.picky;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import il.ac.technion.picky.backend.LoginTask;

/**
 * A login screen
 */
public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_login);
        // force rtl
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_forward_white_24dp);
        }


        // Get references to views
        final EditText editTextUserName=(EditText)findViewById(R.id.editTextUserNameToLogin);
        final EditText editTextPassword=(EditText)findViewById(R.id.editTextPasswordToLogin);

        ((TextView) findViewById(R.id.activity_login_disclaimer3)).setMovementMethod(LinkMovementMethod.getInstance());

        // Set OnClick Listener on SignUp button
        findViewById(R.id.buttonSignIn).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                String userName = editTextUserName.getText().toString();
                String password = editTextPassword.getText().toString();
                // check if any of the fields is empty
                if (userName.isEmpty() || password.isEmpty())
                    TealSnackbar.show(findViewById(R.id.login_activity_screen), getString(R.string.login_missing_details));
                else if (findViewById(R.id.signinProgressBar).getVisibility() != View.VISIBLE)
                    new LoginTask(LoginActivity.this).execute(userName, password);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            //NavUtils.navigateUpTo(this, new Intent(this, CourseListActivity.class));
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}