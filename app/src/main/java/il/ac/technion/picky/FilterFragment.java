package il.ac.technion.picky;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import il.ac.technion.picky.data.DataContract;
import il.ac.technion.picky.data.DataOpenHelper;
import il.ac.technion.picky.data.DataProvider;
import il.ac.technion.picky.data.Filter;
import il.ac.technion.picky.util.Semester;

public class FilterFragment extends Fragment {

    private CourseListActivity activity;
    private View rootView;
    private Spinner facultiesSpinner;
    private Spinner semestersSpinner;
    private Spinner specialCoursesSpinner;
    private NumberPicker pointsPicker;
    private View examsContainer;
    private RadioButton allExamRadio;
    private RadioButton withExamRadio;
    private RadioButton noExamRadio;
    private View examsDatesRangeContainer;
    private DatePickerTextView fromDateTextView;
    private DatePickerTextView toDateTextView;
    private String examFilterMinDateISO;
    private String examFilterMaxDateISO;

    // set this to false when you call setText and want to prevent ExamDateTextWatcher from running
    private boolean textWatcherEnabled = true;

    public FilterFragment() {
        // Required empty public constructor
    }

    public void refreshSemestersSpinner() {
        ((SimpleCursorAdapter) semestersSpinner.getAdapter()).changeCursor(getSemestersCursor());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_filter, container, false);
        facultiesSpinner = (Spinner) rootView.findViewById(R.id.faculties_spinner);
        semestersSpinner = (Spinner) rootView.findViewById(R.id.semesters_spinner);
        specialCoursesSpinner = (Spinner) rootView.findViewById(R.id.special_courses_spinner);
        pointsPicker = (NumberPicker) rootView.findViewById(R.id.points_picker);
        examsContainer = rootView.findViewById(R.id.exams_container);
        allExamRadio = (RadioButton) rootView.findViewById(R.id.all_exam_radio);
        noExamRadio = (RadioButton) rootView.findViewById(R.id.no_exam_radio);
        withExamRadio = (RadioButton) rootView.findViewById(R.id.with_exam_radio);
        examsDatesRangeContainer = rootView.findViewById(R.id.exams_dates_range_container);
        fromDateTextView = (DatePickerTextView) rootView.findViewById(R.id.exam_from_filter);
        toDateTextView = (DatePickerTextView) rootView.findViewById(R.id.exam_to_filter);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (CourseListActivity) getActivity();
        populateViews();
        registerListeners();
        if (savedInstanceState == null) {
            setupExamsFilter(null);
        } else {
            pointsPicker.setValue(savedInstanceState.getInt("pointsPickerValue"));
            // disable spinners
            ((PositionOnItemSelectedListener) facultiesSpinner.getOnItemSelectedListener()).setPrevPosition(savedInstanceState.getInt("facultiesSpinnerPrevPosition"));
            ((PositionOnItemSelectedListener) semestersSpinner.getOnItemSelectedListener()).setPrevPosition(savedInstanceState.getInt("semestersSpinnerPrevPosition"));
            ((PositionOnItemSelectedListener) specialCoursesSpinner.getOnItemSelectedListener()).setPrevPosition(savedInstanceState.getInt("specialCoursesSpinnerPrevPosition"));
            // restore exam filter state
            int semesterId = savedInstanceState.getInt("semestersSpinnerId");
            setupExamsFilter(semesterId < 0 ? null : new Semester(semesterId), savedInstanceState.getString("fromDateTextView"), savedInstanceState.getString("toDateTextView"));
            if (savedInstanceState.getBoolean("noExam"))
                examsDatesRangeContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("pointsPickerValue", pointsPicker.getValue());
        outState.putInt("semestersSpinnerId", Long.valueOf(semestersSpinner.getSelectedItemId()).intValue());
        outState.putString("fromDateTextView", fromDateTextView.getText().toString());
        outState.putString("toDateTextView", toDateTextView.getText().toString());
        outState.putBoolean("noExam", noExamRadio.isChecked());
        // for disabling spinners after restore
        outState.putInt("facultiesSpinnerPrevPosition", ((PositionOnItemSelectedListener) facultiesSpinner.getOnItemSelectedListener()).getPrevPosition());
        outState.putInt("semestersSpinnerPrevPosition", ((PositionOnItemSelectedListener) semestersSpinner.getOnItemSelectedListener()).getPrevPosition());
        outState.putInt("specialCoursesSpinnerPrevPosition", ((PositionOnItemSelectedListener) specialCoursesSpinner.getOnItemSelectedListener()).getPrevPosition());
        super.onSaveInstanceState(outState);
    }

    private void populateViews() {

        facultiesSpinner.setAdapter(new SimpleCursorAdapter(activity,
                R.layout.simple_spinner_dropdown_item_2, activity.getContentResolver().query(DataProvider.FACULTIES_WITH_ALL_ROW_URI, null, null, null, DataContract.Faculty.NAME),
                new String[]{DataContract.Faculty.NAME, DataContract.Faculty.INITIAL},
                new int[]{android.R.id.text1, android.R.id.text2}, 0));

        Cursor cursor = activity.getContentResolver().query(DataProvider.POINTS_URI, null, null, null, DataContract.Course.POINTS);
        List<String> points = new ArrayList<>();
        points.add(getString(R.string.all));
        while (cursor.moveToNext())
            points.add(cursor.getString(0));
        cursor.close();
        pointsPicker.setMinValue(0);
        pointsPicker.setMaxValue(points.size() - 1);
        pointsPicker.setWrapSelectorWheel(false);
        pointsPicker.setDisplayedValues(points.toArray(new String[points.size()]));

        semestersSpinner.setAdapter(new SimpleCursorAdapter(activity,
                R.layout.simple_spinner_dropdown_item_2, getSemestersCursor(),
                new String[]{DataContract.Faculty.NAME},
                new int[]{android.R.id.text1}, 0));

        specialCoursesSpinner.setAdapter(new ArrayAdapter<>(activity, R.layout.simple_spinner_dropdown_item_2, android.R.id.text1, getResources().getStringArray(R.array.filter_special_courses)));
    }

    private Cursor getSemestersCursor() {
        return activity.getContentResolver().query(DataProvider.SEMESTERS_WITH_ALL_ROW_URI, null, null, null, null);
    }

    private void registerListeners() {
        facultiesSpinner.setOnItemSelectedListener(new PositionOnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isPositionChanged(position))
                    return;
                if (id < 0)
                    activity.getFilter().remove(Filter.FilterKey.FACULTY);
                else
                    activity.getFilter().add(Filter.FilterKey.FACULTY, String.valueOf(id));
                activity.refreshList("FACULTY");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        pointsPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal == 0)
                    activity.getFilter().remove(Filter.FilterKey.POINTS);
                else
                    activity.getFilter().add(Filter.FilterKey.POINTS, picker.getDisplayedValues()[newVal]);
                activity.refreshList("POINTS");
            }
        });

        specialCoursesSpinner.setOnItemSelectedListener(new PositionOnItemSelectedListener() {

            private final Filter.FilterKey[] filters = {Filter.FilterKey.PROJECT, Filter.FilterKey.SEMINAR, Filter.FilterKey.LAB, Filter.FilterKey.SPORT};
            private Filter.FilterKey filterKey;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isPositionChanged(position))
                    return;
                if (filterKey != null)
                    activity.getFilter().remove(filterKey);
                if (position > 0) {
                    filterKey = filters[position - 1];
                    activity.getFilter().add(filterKey);
                }
                activity.refreshList("SPECIAL COURSES");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        semestersSpinner.setOnItemSelectedListener(new PositionOnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isPositionChanged(position))
                    return;
                // if "all" was selected is or specific semester
                Semester semester = id < 0 ? null : new Semester((int) id);
                if (semester == null) {
                    activity.getFilter().remove(Filter.FilterKey.SEMESTER);
                } else {
                    activity.getFilter().add(Filter.FilterKey.SEMESTER, String.valueOf(semester.getYear()), String.valueOf(semester.getSemester()));
                }
                setupExamsFilter(semester);
                // if semester was selected set to exam mode
                activity.setExamMode(semester != null);
                activity.refreshList("SEMESTERS");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        allExamRadio.setOnClickListener(examsRadioClickListener);
        withExamRadio.setOnClickListener(examsRadioClickListener);
        noExamRadio.setOnClickListener(examsRadioClickListener);

        fromDateTextView.addTextChangedListener(new ExamDateTextWatcher(fromDateTextView));
        toDateTextView.addTextChangedListener(new ExamDateTextWatcher(toDateTextView));

        rootView.findViewById(R.id.clear_filter_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // disable spinners listeners
                ((PositionOnItemSelectedListener) facultiesSpinner.getOnItemSelectedListener()).setPrevPosition(0);
                ((PositionOnItemSelectedListener) semestersSpinner.getOnItemSelectedListener()).setPrevPosition(0);
                ((PositionOnItemSelectedListener) specialCoursesSpinner.getOnItemSelectedListener()).setPrevPosition(0);
                // reset views
                facultiesSpinner.setSelection(0);
                semestersSpinner.setSelection(0);
                specialCoursesSpinner.setSelection(0);
                pointsPicker.setValue(0);
                setupExamsFilter(null);
                activity.setExamMode(false);
                // clear filter
                activity.clearFilterFragmentFilter();
                activity.refreshList("clearFilter");
            }
        });
    }

    private void setupExamsFilter(Semester semester) {
        setupExamsFilter(semester, "", "");
    }

    private void setupExamsFilter(Semester semester, String from, String to) {
        if (semester != null) {
            activity.getFilter().remove(Filter.FilterKey.EXAMS_DATES_RANGE_OR_NO_EXAM);
            DataOpenHelper db = new DataOpenHelper(activity);
            Cursor c = db.getExamsMinMaxISODates(semester.getYear(), semester.getSemester());
            c.moveToNext();
            examFilterMinDateISO = c.getString(0);
            examFilterMaxDateISO = c.getString(1);
            String minDate = DatePickerTextView.fromISODate(examFilterMinDateISO);
            String maxDate = DatePickerTextView.fromISODate(examFilterMaxDateISO);
            textWatcherEnabled = false;
            fromDateTextView.resetDatePicker().setMinDate(minDate).setMaxDate(maxDate).setInitialDate(from.isEmpty() ? minDate : from).setText(from);
            toDateTextView.resetDatePicker().setMinDate(minDate).setMaxDate(maxDate).setInitialDate(to.isEmpty() ? maxDate : to).setText(to);
            textWatcherEnabled = true;
            db.close();
            examsDatesRangeContainer.setVisibility(View.VISIBLE);
            examsContainer.setVisibility(View.VISIBLE);
        } else {
            allExamRadio.setChecked(true);
            examsContainer.setVisibility(View.GONE);
            activity.getFilter().remove(Filter.FilterKey.HAS_EXAM);
            activity.getFilter().remove(Filter.FilterKey.EXAMS_DATES_RANGE_OR_NO_EXAM);
        }
    }

    private final View.OnClickListener examsRadioClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!((RadioButton) view).isChecked())
                return;
            switch (view.getId()) {
                case R.id.all_exam_radio:
                    activity.getFilter().remove(Filter.FilterKey.HAS_EXAM);
                    updateExamsDatesFilter();
                    examsDatesRangeContainer.setVisibility(View.VISIBLE);
                    break;
                case R.id.with_exam_radio:
                    activity.getFilter().add(Filter.FilterKey.HAS_EXAM, "1");
                    updateExamsDatesFilter();
                    examsDatesRangeContainer.setVisibility(View.VISIBLE);
                    break;
                case R.id.no_exam_radio:
                    activity.getFilter().add(Filter.FilterKey.HAS_EXAM, "0");
                    activity.getFilter().remove(Filter.FilterKey.EXAMS_DATES_RANGE_OR_NO_EXAM);
                    examsDatesRangeContainer.setVisibility(View.GONE);
                    break;
            }
            activity.refreshList("HAS_EXAM");
        }
    };

    /**
     * Dates in format dd/MM/yyyy e.g. 31/12/2015
     *
     * @param to
     * @param from
     * @return true if to<=from or one of the dates is empty string
     */
    private boolean isValidDateRange(String from, String to) {
        if (from == null || to == null)
            return true;
        return from.isEmpty() || to.isEmpty() || !DatePickerTextView.parse(from).after(DatePickerTextView.parse(to));
    }

    private void updateExamsDatesFilter() {
        updateExamsDatesFilter(null);
    }

    private void updateExamsDatesFilter(TextView changedTextView) {
        String from = fromDateTextView.getText().toString();
        String to = toDateTextView.getText().toString();
        if (!isValidDateRange(from, to)) {
            TealSnackbar.show(rootView, getString(R.string.exams_dates_range_error));
            if (changedTextView != null) {
                textWatcherEnabled = false;
                changedTextView.setText("");
                textWatcherEnabled = true;
            }
            return;
        }
        if (from.isEmpty() && to.isEmpty()) {
            activity.getFilter().remove(Filter.FilterKey.EXAMS_DATES_RANGE_OR_NO_EXAM);
            return;
        }
        from = from.isEmpty() ? examFilterMinDateISO : DatePickerTextView.toISODate(from);
        to = to.isEmpty() ? examFilterMaxDateISO : DatePickerTextView.toISODate(to);
        activity.getFilter().add(Filter.FilterKey.EXAMS_DATES_RANGE_OR_NO_EXAM, from, to, from, to);
    }

    /**
     * to disable onItemSelected e.g. in initialization call isPositionChanged at the beginning of your onItemSelected implementation
     */
    private static abstract class PositionOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        private int prevPosition;

        public int getPrevPosition() {
            return prevPosition;
        }

        public void setPrevPosition(int prevPosition) {
            this.prevPosition = prevPosition;
        }

        /**
         * This also sets prevPosition to position
         *
         * @param position
         * @return
         */
        public boolean isPositionChanged(int position) {
            boolean b = this.prevPosition != position;
            prevPosition = position;
            return b;
        }
    }

    private class ExamDateTextWatcher implements TextWatcher {

        private final TextView textView;

        public ExamDateTextWatcher(TextView textView) {
            this.textView = textView;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!textWatcherEnabled)
                return;
            updateExamsDatesFilter(textView);
            activity.refreshList("EXAMS_DATES_RANGE_OR_NO_EXAM");
        }
    }

}
