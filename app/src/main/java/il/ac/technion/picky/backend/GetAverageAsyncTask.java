package il.ac.technion.picky.backend;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;

import com.appspot.pickybackend.picky.model.CourseStats;

import static il.ac.technion.picky.backend.ApiService.API;

public class GetAverageAsyncTask extends AsyncTask<Long, Void, CourseStats> {

    private int minRunningTime = 1250;
    private long startTime;
    private OnPostExecuteListener listener;
    // Throwable (and not Exception) because unauthorized API returns Error
    private Throwable throwable;

    @Override
    protected void onPreExecute() {
        startTime = SystemClock.uptimeMillis();
    }

    @Override
    protected CourseStats doInBackground(Long... params) {
        CourseStats stats = null;
        try {
            stats = API.getStats(params[0]).execute();
        } catch (Throwable e) {
            throwable = e;
        }
        return stats;
    }

    @Override
    protected void onPostExecute(final CourseStats stats) {
        long runningTime = SystemClock.uptimeMillis() - startTime;
        if (minRunningTime > 0 && runningTime < minRunningTime)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    listener.onPostExecute(stats, throwable);
                }
            }, minRunningTime - runningTime);
        else
            listener.onPostExecute(stats, throwable);
    }

    public GetAverageAsyncTask setOnPostExecuteListener(OnPostExecuteListener listener) {
        this.listener = listener;
        return this;
    }

    /**
     * set minimum running time for this asynctask
     *
     * @param minRunningTime in milliseconds
     * @return
     */
    public GetAverageAsyncTask setMinRunningTime(int minRunningTime) {
        this.minRunningTime = minRunningTime;
        return this;
    }

    public interface OnPostExecuteListener {

        void onPostExecute(CourseStats stats, Throwable throwable);
    }
}
