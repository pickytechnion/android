package il.ac.technion.picky.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.Collection;

import il.ac.technion.picky.util.RepParser;

public class DataOpenHelper extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "picky.db";
    private static final int DATABASE_VERSION = 5;

    private static final String REP_COURSES_CHANGES_QUERY = "SELECT _id,number,name,points,faculty FROM rep EXCEPT SELECT _id,number,name,points,faculty FROM courses";
    private static final String REP_COURSES_BY_SEMESTER_CHANGES_QUERY = "SELECT _id,%1$s AS year,%2$s AS semester,examA IS NOT NULL OR examB IS NOT NULL AS hasExam,examA,examB FROM rep EXCEPT SELECT _id,year,semester,hasExam,examAISO,examBISO FROM courses_by_semesters WHERE year=%1$s AND semester=%2$s";
    private static final String REP_COURSES_BY_SEMESTER_REMOVED_IDS_QUERY = "SELECT _id FROM courses_by_semesters WHERE year=%s AND semester=%s EXCEPT SELECT _id FROM rep";

    public static String getDBVersion() {
        return String.valueOf(DATABASE_VERSION);
    }

    public DataOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // overwrite existing db when DATABASE_VERSION is incremented
        setForcedUpgrade();
    }

    /**
     * Most recent semester in courses_by_semester
     *
     * @return Integer in format e.g. 20151 for winter 2015. Returns 0 if table empty.
     */
    public int getMaxSemester() {
        return (int) getReadableDatabase().compileStatement("SELECT MAX(year||semester) FROM courses_by_semesters").simpleQueryForLong();
    }

    public int deleteRepData() {
        return getWritableDatabase().compileStatement("DELETE FROM rep").executeUpdateDelete();
    }

    public int insertRepData(Collection<RepParser.Faculty> faculties) {
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement statement = db.compileStatement("INSERT INTO rep(_id ,number,name,points,faculty,examA,examB) VALUES (?,?,?,?,?,?,?)");
        db.beginTransaction();
        int count = 0;
        for (RepParser.Faculty faculty : faculties) {
            for (RepParser.Course course : faculty.getCourses()) {
                statement.clearBindings();
                statement.bindLong(1, Integer.parseInt(course.getNumber()));
                statement.bindString(2, course.getNumber());
                statement.bindString(3, course.getName());
                statement.bindDouble(4, course.getPoints());
                statement.bindLong(5, faculty.getNumber());
                bindStringOrNull(statement, 6, course.getExamA());
                bindStringOrNull(statement, 7, course.getExamB());
                statement.executeInsert();
                count++;
            }
        }
        // commit
        db.setTransactionSuccessful();
        db.endTransaction();
        return count;
    }

    public Cursor getRepCoursesChanges() {
        return getReadableDatabase().rawQuery(REP_COURSES_CHANGES_QUERY, null);
    }

    public int updateCoursesByRep() {
        return getWritableDatabase().compileStatement("REPLACE INTO courses " + DataOpenHelper.REP_COURSES_CHANGES_QUERY).executeUpdateDelete();
    }

    public Cursor getRepCoursesBySemestersRemoved(int year, int semester) {
        return getReadableDatabase().rawQuery(String.format(REP_COURSES_BY_SEMESTER_REMOVED_IDS_QUERY, year, semester), null);
    }

    public int deleteRemovedCoursesBySemestersByRep(int year, int semester) {
        return getWritableDatabase().compileStatement("DELETE FROM courses_by_semesters WHERE _id IN (" + String.format(REP_COURSES_BY_SEMESTER_REMOVED_IDS_QUERY, year, semester) + ")").executeUpdateDelete();
    }

    public Cursor getRepCoursesBySemestersChanges(int year, int semester) {
        return getReadableDatabase().rawQuery(String.format(REP_COURSES_BY_SEMESTER_CHANGES_QUERY, year, semester), null);
    }

    public int updateCoursesBySemestersByRep(int year, int semester) {
        return getWritableDatabase().compileStatement("REPLACE INTO courses_by_semesters " + String.format(REP_COURSES_BY_SEMESTER_CHANGES_QUERY, year, semester)).executeUpdateDelete();
    }

    public Cursor getExamsMinMaxISODates(int year, int semester) {
        return getReadableDatabase().rawQuery("SELECT MIN(examAISO), MAX(examBISO) FROM courses_by_semesters WHERE year=? AND semester=?", new String[]{String.valueOf(year), String.valueOf(semester)});
    }

    private void bindStringOrNull(SQLiteStatement statement, int index, String value) {
        if (value == null)
            statement.bindNull(index);
        else
            statement.bindString(index, value);
    }

}
