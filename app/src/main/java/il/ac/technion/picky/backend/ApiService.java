package il.ac.technion.picky.backend;

import android.util.Log;

import com.appspot.pickybackend.picky.Picky;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.Collections;

import il.ac.technion.picky.PickyApplication;
import il.ac.technion.picky.R;

public class ApiService {

    public static final Picky API;

    static {
        PrivateKey serviceAccountPrivateKey = null;
        try {
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            keystore.load(PickyApplication.getContext().getResources().openRawResource(R.raw.key), Constants.SECRET.toCharArray());
            serviceAccountPrivateKey = (PrivateKey) keystore.getKey(Constants.PRIVATE_KEY, Constants.SECRET.toCharArray());
        } catch (Exception e) {
            Log.e("keyProblem", e.getMessage());
        }
        Credential credential = new GoogleCredential.Builder()
                .setTransport(AndroidHttp.newCompatibleTransport())
                .setJsonFactory(JacksonFactory.getDefaultInstance())
                .setServiceAccountPrivateKey(serviceAccountPrivateKey)
                .setServiceAccountId(Constants.ACCOUNT_ID)
                .setServiceAccountScopes(Collections.singleton(Constants.EMAIL_SCOPE))
                .build();

        API = new Picky.Builder(AndroidHttp.newCompatibleTransport(),
                new AndroidJsonFactory(), credential).setApplicationName("picky").build();
    }

    public static boolean isOverQuotaException(GoogleJsonResponseException e) {
        return e.getDetails().getMessage().contains("OverQuotaException");
    }
}
