package il.ac.technion.picky.backend;

import android.app.Activity;
import android.os.AsyncTask;

import com.appspot.pickybackend.picky.model.Result;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

import java.io.IOException;

import il.ac.technion.picky.R;
import il.ac.technion.picky.TealSnackbar;

import static il.ac.technion.picky.backend.ApiService.API;

public class PutPublishedAverageTask extends AsyncTask<PubReportWrapper, Void, Result> {

    private final Activity activity;

    public PutPublishedAverageTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected Result doInBackground(PubReportWrapper... params) {

        Result result = new Result().setSuccess(false);
        try {
            PubReportWrapper pubReportWrapper = params[0];
            result = API.addWaitingPubReport(pubReportWrapper.getPubReport().getSource(),
                    pubReportWrapper.getCourseId(), pubReportWrapper.getPubReport().getFinalAvg(),
                    pubReportWrapper.getYear(), pubReportWrapper.getSemester(),
                    pubReportWrapper.getPubReport().getMoed()).setComments(pubReportWrapper.getPubReport().getComments()).setSourceString(pubReportWrapper.getPubReport().getSourceString()).execute();
            result.setMessage(activity.getString(R.string.report_uploaded));
        } catch (GoogleJsonResponseException e) {
            if (ApiService.isOverQuotaException(e))
                result.setMessage(activity.getString(R.string.backend_over_quota));
            else
                result.setMessage(activity.getString(R.string.backend_error));
        } catch (IOException e) {
            result.setMessage(activity.getString(R.string.network_problem));
        } catch (Exception e) {
            result.setMessage(activity.getString(R.string.unknown_problem));
        }
        return result;
    }

    @Override
    protected void onPostExecute(Result result) {
        TealSnackbar.show(activity.findViewById(R.id.course_detail_container), result.getMessage());
    }
}

