package il.ac.technion.picky.util;

public class Semester {

    private static final String[] HEBREW_NAMES = new String[]{"חורף", "אביב", "קיץ"};
    private final int year;
    private final int semester;

    /**
     *
     * @param id
     *    e.g. 20151 for winter 2014/2015
     */
    public Semester(int id) {
        this.year = id/10;
        this.semester = id%10;
    }

    public int getYear() {
        return year;
    }

    public int getSemester() {
        return semester;
    }

    public String toHebString() {
        return HEBREW_NAMES[semester-1] + " " + (semester==1? year-1 + "/":"")+ year;
    }
}
