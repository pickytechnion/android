package il.ac.technion.picky;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.List;

import il.ac.technion.picky.data.DataContract;
import il.ac.technion.picky.data.DataProvider;
import il.ac.technion.picky.data.Filter;
import il.ac.technion.picky.data.RepUpdater;

/**
 * A list fragment representing a list of Courses. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link CourseDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class CourseListFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        void onItemSelected(long id, String number, String name);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */

    private static final Callbacks DUMMY_CALLBACKS = new Callbacks() {
        @Override
        public void onItemSelected(long id, String number, String name) {
        }
    };

    private final String[] EXAMS_PROJECTION = {"*", "ifnull(strftime('%d/%m/%Y'," + DataContract.CourseWithExams.EXAM_A_ISO + "),'" + PickyApplication.getContext().getString(R.string.none) + "') AS " + DataContract.CourseWithExams.EXAM_A, "ifnull(strftime('%d/%m/%Y'," + DataContract.CourseWithExams.EXAM_B_ISO + "),'" + PickyApplication.getContext().getString(R.string.none) + "') AS " + DataContract.CourseWithExams.EXAM_B};

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks callbacks = DUMMY_CALLBACKS;
    private SimpleCursorAdapter adapter;
    private Uri uri;
    private String[] projection;
    private TextView listCountView;
    private CourseListActivity activity;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CourseListFragment() {
    }

    public void setCoursesAdapter() {
        adapter = new SimpleCursorAdapter(getActivity(),
                R.layout.courses_list_item, null,
                new String[]{DataContract.Course.NUMBER, DataContract.Course.NAME, DataContract.Course.POINTS, DataContract.CourseWithFacultyInitial.FACULTY_INITIAL},
                new int[]{R.id.course_number, R.id.course_name, R.id.course_points, R.id.course_faculty}, 0);
        setListAdapter(adapter);
        uri = DataProvider.COURSES_WITH_FACULTY_INITIAL_URI;
        projection = null;
    }

    public void setExamsAdapter() {
        adapter = new SimpleCursorAdapter(getActivity(),
                R.layout.courses_list_exams_item, null,
                new String[]{DataContract.Course.NUMBER, DataContract.Course.NAME, DataContract.Course.POINTS, DataContract.CourseWithFacultyInitial.FACULTY_INITIAL, DataContract.CourseWithExams.EXAM_A, DataContract.CourseWithExams.EXAM_B},
                new int[]{R.id.course_number, R.id.course_name, R.id.course_points, R.id.course_faculty, R.id.course_exam_a, R.id.course_exam_b}, 0);
        setListAdapter(adapter);
        uri = DataProvider.COURSES_WITH_EXAMS_URI;
        projection = EXAMS_PROJECTION;
    }

    private SwipeRefreshLayout mSwipeRefreshLayout;

    public boolean canScrollVertically(int d) {
        return true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        activity = (CourseListActivity) getActivity();
        listCountView = new TextView(getActivity());
        listCountView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        listCountView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        listCountView.setGravity(Gravity.RIGHT);
        final ListView listView = getListView();
        listView.addHeaderView(listCountView, null, false);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setSelected(true);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {

                List<String> semesterArgs = activity.getFilter()==null ? null : activity.getFilter().getArgs(Filter.FilterKey.SEMESTER);
                startActivity(new Intent(Intent.ACTION_VIEW,
                       Uri.parse("http://www.graduate.technion.ac.il/heb/subjects/?SUB=" + String.valueOf(id) +
                               ((semesterArgs==null || semesterArgs.size()<2) ? "" : String.format("&SEM=%d0%s",Integer.parseInt(semesterArgs.get(0)) - 1, semesterArgs.get(1))))));
                return true;
            }
        });

        // set here and not in activity OnCreate because in Android 4 there is exception: Cannot add header view to list -- setAdapter has already been called
        activity.setAdapters();
        // populate list
        restartLoader();

        // Swipe to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipe_to_refresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new RepUpdater((CourseListActivity) getActivity()).execute();
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                mSwipeRefreshLayout.setEnabled(!listView.canScrollVertically(-1));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        return new CursorLoader(
                activity,   // Parent activity context
                uri, // Table to query
                projection,                       // Projection to return
                activity.getFilter().getSelection(),      // selection clause
                activity.getFilter().getSelectionArgs(),  // selection arguments
                activity.getSort()                        // sort order
        );

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
        listCountView.setText(String.format(getString(R.string.courses_count), adapter.getCount()));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Activities containing this fragment must implement its callbacks.
        if (!(context instanceof Callbacks))
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        callbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Reset the active callbacks interface to the dummy implementation.
        callbacks = DUMMY_CALLBACKS;
    }


    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        if (((CourseListActivity) getActivity()).isTwoPane())
            view.setSelected(true);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        //
        // (position - 1) because of list count header
        Cursor item = (Cursor) adapter.getItem(position - 1);
        callbacks.onItemSelected(id, item.getString(1), item.getString(2));
    }

    public void restartLoader() {
        getLoaderManager().restartLoader(0, null, this);
    }
}

