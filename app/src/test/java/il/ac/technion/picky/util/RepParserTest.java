package il.ac.technion.picky.util;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class RepParserTest {

    @Test
    public void generalTest() throws Exception {
        RepParser parser = RepParser.fromZipFile(RepParserTest.class.getResource("/REPY_16w.zip").getFile());
        RepParser.Semester semester = parser.getSemester();
        assertEquals("חורף תשע\"ו", semester.getHebName());
        assertEquals(2016, semester.getYear());
        assertEquals(1, semester.getSemester());
        assertEquals(18, parser.getFaculties().size());
        RepParser.Faculty faculty = parser.getFaculties().get(23);
        assertEquals("מדעי המחשב", faculty.getName());
        RepParser.Course course = faculty.getCourses().get(1);
        assertEquals("234107", course.getNumber());
        assertEquals("2016-02-09", course.getExamA());
    }


}
