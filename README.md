
# Overview #

Android application that display averages of Technion courses.
For more info visit our [website](http://picky-technion.appspot.com).

# Build #

If you try to build the app you will get some errors because some files are missing.
This files are needed to protect the server from unautorized requests.
To build without errors you need to copy the content of `patch` directory to the root directory of the project.
This will enable you to use the app but without making requests to the server (app may crash when attempting to do so).

# License #

 Copyright (C) 2015 Ohad Eytan, Erez Fridman, Ohad Ben.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.