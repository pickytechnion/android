package il.ac.technion.picky.backend;

/**
 * Contains the client IDs and scopes for allowed clients consuming your API.
 */
public class Constants {
  public static final String SERVICE_ACCOUNT_CLIENT_ID = "";
}
