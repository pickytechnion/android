package il.ac.technion.picky.backend;

/**
 * Created by ohad on 3/20/15.
 */

public class Constants {
    public static final String PRIVATE_KEY = "";
    public static final String SECRET = "";
    public static final String ACCOUNT_ID = "";
    public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
}